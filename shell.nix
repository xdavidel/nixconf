# Shell for bootstrapping flake-enabled nix and other tooling
{
  pkgs ?
  # If pkgs is not defined, instanciate nixpkgs from locked commit
  let
    lock = (builtins.fromJSON (builtins.readFile ./flake.lock)).nodes.nixpkgs.locked;
    nixpkgs = fetchTarball {
      url = "https://github.com/nixos/nixpkgs/archive/${lock.rev}.tar.gz";

      sha256 = lock.narHash;
    };
  in
    import nixpkgs {
      overlays = [
        (final: _prev: import ./pkgs {pkgs = final;})
      ];
    },
  ...
}: let
  conf-json = (builtins.fromJSON (builtins.readFile ./flake.lock)).nodes.nvim;
  locked = conf-json.locked;
  original = conf-json.original;
  nvim = pkgs.fetchgit {
    url = "https://${original.type}.com/${original.owner}/${original.repo}.git";
    rev = locked.rev;
    sha256 = locked.narHash;
  };
in {
  default = pkgs.mkShell {
    NIX_CONFIG = "extra-experimental-features = nix-command flakes";
    nativeBuildInputs = with pkgs; [
      git
      home-manager
      nvim
      nix
      pfetch
    ];
    shellHook = ''
      pfetch
    '';
  };
}
