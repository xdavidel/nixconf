{
  description = "Modular nix configuration";

  nixConfig = {
    extra-substituters = [
      "https://cosmic.cachix.org/"
    ];
    extra-trusted-public-keys = [
      "cosmic.cachix.org-1:Dya9IyXD4xdBehWjrkPv6rtxpmMdRel02smYzA85dPE="
    ];
  };

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixpkgs-unstable";

    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nix-colors.url = "github:misterio77/nix-colors";

    firefox-addons = {
      url = "gitlab:rycee/nur-expressions?dir=pkgs/firefox-addons";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    cosmic = {
      url = "github:lilyinstarlight/nixos-cosmic";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };

    nvim.url = "gitlab:xdavidel/nvim";
  };

  outputs = {...} @ inputs: let
    # super simple boilerplate-reducing
    # lib with a bunch of functions
    mLib = import ./mlib/default.nix {inherit inputs;};
  in
    with mLib; {
      # usage: `nix build .#<package>` / via inputs
      packages = forAllSystems (system: import ./pkgs {pkgs = pkgsFor system;});

      # usage: `nix fmt`
      formatter = forAllSystems (system: inputs.nixpkgs.legacyPackages.${system}.alejandra);

      # custom packages and modifications, exported as overlays
      overlays = import ./overlays {inherit inputs;};

      # usage: `nix develop / nix-shell`
      devShells = forAllSystems (system: import ./shell.nix {pkgs = pkgsFor system;});

      # usage: `nixos-rebuild --flake .#<host> switch`
      nixosConfigurations = {
        t14s = mkSystem ./hosts/t14s;
        work = mkSystem ./hosts/work;
        hptower = mkSystem ./hosts/hptower;
      };

      # usage: `nix run .#homeConfigurations.x86_64-linux.user.activationPackage`
      homeConfigurations = let
        user = "user";
      in
        forAllSystems (system: {
          "${user}" = mkHome system user ./hosts/hm;
        });

      nixosModules.default = ./modules/nixos;
      homeManagerModules.default = ./modules/homemanager;
    };
}
