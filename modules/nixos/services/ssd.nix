{
  config,
  pkgs,
  inputs,
  lib,
  ...
}: {
  services.fstrim.enable = true;
}
