{
  config,
  pkgs,
  lib,
  inputs,
  ...
}: {
  services.greetd = let
    user = lib.toLower config.mNixOS.userName;
  in {
    enable = true;
    settings = {
      default_session = let
        greeter = "${pkgs.greetd.tuigreet}/bin/tuigreet";
      in {
        inherit user;
        command = builtins.concatStringsSep " " [
          greeter
          "--remember"
          "--remember-user-session"
        ];
      };
    };
  };
}
