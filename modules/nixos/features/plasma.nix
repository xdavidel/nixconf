{
  config,
  pkgs,
  lib,
  ...
}: let
  user = lib.toLower config.mNixOS.userName;
in {
  # Enable the KDE Plasma Desktop Environment.
  services.desktopManager.plasma6.enable = true;
  services.displayManager.sddm.enable = true;
  services.displayManager.defaultSession = "plasmax11";

  services.xrdp.enable = true;
  services.xrdp.defaultWindowManager = "startplasma-x11";
  services.xrdp.openFirewall = true;

  # Enable automatic login for the user.
  services.displayManager.autoLogin = {
    enable = true;
    user = "${user}";
  };

  services.fwupd.enable = true;

  environment.systemPackages = with pkgs; [
    maliit-keyboard
    maliit-framework
    onboard

    # device settings and information
    pciutils
    clinfo
    vulkan-tools
    virtualgl
  ];
}
