{
  config,
  pkgs,
  lib,
  inputs,
  ...
}: {
  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    autorun = false;
    xkb = {
      options = "grp:alt_shift_toggle,caps:escape";
      layout = lib.mkForce "us,il";
    };
    autoRepeatDelay = 300;
    autoRepeatInterval = 50;
    displayManager = {
      startx.enable = true;
      sessionCommands = ''
        ${pkgs.xorg.xset}/bin/xset r rate 300 50
        ${pkgs.xorg.setxkbmap}/bin/setxkbmap -layout us,il -option grp:alt_shift_toggle,caps:escape
      '';
    };
  };

  environment.variables = {
    XCURSOR_SIZE = 16;
  };

  environment.systemPackages = with pkgs; [
    xclip
    feh
    arandr
    xorg.xset
    xorg.setxkbmap
    xcompmgr
  ];
}
