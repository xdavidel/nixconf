{
  config,
  pkgs,
  inputs,
  lib,
  ...
}: let
  user = lib.toLower config.mNixOS.userName;
in {
  # Enable networking
  networking.networkmanager.enable = true;

  users.users.${user}.extraGroups = ["networkmanager"];
}
