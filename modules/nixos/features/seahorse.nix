{
  config,
  pkgs,
  inputs,
  ...
}: {
  programs.seahorse.enable = true;
}
