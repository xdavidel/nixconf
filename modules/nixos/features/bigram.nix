{
  config,
  pkgs,
  inputs,
  lib,
  ...
}: {
  # use tmpfs
  boot.tmp.useTmpfs = true;
  boot.tmp.tmpfsSize = "25%";

  zramSwap.enable = true;
}
