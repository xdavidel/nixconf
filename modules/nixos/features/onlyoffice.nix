{
  config,
  pkgs,
  inputs,
  lib,
  ...
}: {
  environment.systemPackages = [pkgs.onlyoffice-bin];

  # Allow installation of unfree corefonts package
  nixpkgs.config.allowUnfreePredicate = pkg:
    builtins.elem (lib.getName pkg) ["corefonts"];

  fonts.packages = with pkgs; [
    corefonts
  ];
}
