{
  config,
  pkgs,
  inputs,
  ...
}: {
  services.logmein-hamachi.enable = true;
  programs.haguichi.enable = true;

  # To run Hamachi without root - add / edit the file:
  # /var/lib/logmein-hamachi/h2-engine-override.cfg
  # With the content:
  # `Ipc.User username`
}
