{
  config,
  pkgs,
  inputs,
  lib,
  ...
}: {
  users.defaultUserShell = pkgs.zsh;
  programs.zsh.enable = true;
}
