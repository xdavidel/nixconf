{
  pkgs,
  config,
  lib,
  inputs,
  outputs,
  mLib,
  ...
}: let
  cfg = config.mNixOS;

  # Taking all modules in ./features and adding enables to them
  features =
    mLib.extendModules
    (name: {
      extraOptions = {
        mNixOS.${name}.enable = lib.mkEnableOption "enables ${name} configuration";
      };

      configExtension = config: (lib.mkIf cfg.${name}.enable config);
    })
    (mLib.filesIn ./features);

  # Taking all module bundles in ./bundles and adding bundle.enables to them
  bundles =
    mLib.extendModules
    (name: {
      extraOptions = {
        mNixOS.bundles.${name}.enable = lib.mkEnableOption "enables ${name} module bundle";
      };

      configExtension = config: (lib.mkIf cfg.bundles.${name}.enable config);
    })
    (mLib.filesIn ./bundles);

  # Taking all module services in ./services and adding services.enables to them
  services =
    mLib.extendModules
    (name: {
      extraOptions = {
        mNixOS.services.${name}.enable = lib.mkEnableOption "enables ${name} service";
      };

      configExtension = config: (lib.mkIf cfg.services.${name}.enable config);
    })
    (mLib.filesIn ./services);
in {
  imports =
    [
      inputs.home-manager.nixosModules.home-manager
    ]
    ++ features
    ++ bundles
    ++ services;

  config = {
    nix.settings.experimental-features = ["nix-command" "flakes"];
    programs.nix-ld.enable = true;

    nixpkgs = {
      # You can add overlays here
      overlays = [
        # Add overlays your own flake exports (from overlays and pkgs dir):
        outputs.overlays.additions
        outputs.overlays.modifications
        outputs.overlays.unstable-packages
      ];
      # Configure your nixpkgs instance
      config = {
        # Disable if you don't want unfree packages
        allowUnfree = true;
      };
    };
  };
}
