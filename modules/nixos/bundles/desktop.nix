{
  config,
  pkgs,
  inputs,
  ...
}: {
  # register appimages
  boot.binfmt.registrations.appimage = {
    wrapInterpreterInShell = false;
    interpreter = "${pkgs.appimage-run}/bin/appimage-run";
    recognitionType = "magic";
    offset = 0;
    mask = ''\xff\xff\xff\xff\x00\x00\x00\x00\xff\xff\xff'';
    magicOrExtension = ''\x7fELF....AI\x02'';
  };

  boot.binfmt.emulatedSystems = [
    "aarch64-linux"
    "armv6l-linux"
    "armv7l-linux"
    "x86_64-windows"
  ];

  # Select internationalisation properties.
  i18n = let
    LC_Variables = "en_US.UTF-8";
  in {
    defaultLocale = "${LC_Variables}";

    extraLocaleSettings = {
      LC_ADDRESS = "${LC_Variables}";
      LC_IDENTIFICATION = "${LC_Variables}";
      LC_MEASUREMENT = "${LC_Variables}";
      LC_MONETARY = "${LC_Variables}";
      LC_NAME = "${LC_Variables}";
      LC_NUMERIC = "${LC_Variables}";
      LC_PAPER = "${LC_Variables}";
      LC_TELEPHONE = "${LC_Variables}";
      LC_TIME = "${LC_Variables}";
    };
  };

  services.libinput.enable = true;

  security.polkit.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable gnome-keyring
  services.gnome.gnome-keyring.enable = true;

  # thumbnailer service
  services.tumbler.enable = true;

  services.gvfs.enable = true;

  boot.plymouth.enable = true;

  # Enable sound with pipewire.
  hardware.pulseaudio.enable = false;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  security.rtkit.enable = true;

  programs = {
    thunar = {
      enable = true;
      plugins = with pkgs.xfce; [
        thunar-volman
      ];
    };
  };

  programs.file-roller.enable = true;

  fonts.packages = with pkgs; [
    (nerdfonts.override {fonts = ["CascadiaCode"];})
  ];

  environment.systemPackages = with pkgs; [
    ntfs3g
  ];
}
