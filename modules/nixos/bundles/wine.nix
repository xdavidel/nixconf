{
  config,
  pkgs,
  inputs,
  lib,
  ...
}: {
  environment.systemPackages = with pkgs; [
    bottles
    wineWowPackages.stable
    wineWowPackages.waylandFull
    winetricks
  ];
}
