{
  config,
  pkgs,
  inputs,
  lib,
  ...
}: let
  pypkgs = ps:
    with ps; [
      pandas
      requests
      ipython
    ];
in {
  environment.systemPackages =
    (with pkgs; [
      man-pages-posix
      gnumake
      cmake
      pipx
      sshfs
      (python3.withPackages pypkgs)
    ])
    ++ (with pkgs.unstable; [
      ]);

  programs.screen = {
    enable = true;
    screenrc =
      /*
      sh
      */
      ''
        # the following two lines give a two-line status, with the current window highlighted
        hardstatus alwayslastline
        hardstatus string '%{= kG}[%{G}%H%? %1`%?%{g}][%= %{= kw}%-w%{+b yk} %n*%t%?(%u)%? %{-}%+w %=%{g}][%{B}%m/%d %{W}%C%A%{g}]'

        # no welcome message
        startup_message off

        # 256 colors
        attrcolor b ".I"
        termcapinfo xterm 'Co#256:AB=\E[48;5;%dm:AF=\E[38;5;%dm'
        defbce on

        # mouse tracking allows to switch region focus by clicking (prevent scrolling)
        # mousetrack on

        # huge scrollback buffer
        defscrollback 5000

        # mouse scrolling
        termcapinfo xterm* ti@:te@
      '';
  };

  # development utilities man-pages
  documentation.dev.enable = true;
}
