{
  pkgs,
  options,
  lib,
  ...
}: let
  libs = with pkgs; [
    alsa-lib
    at-spi2-atk
    at-spi2-core
    curl
    dbus
    expat
    fontconfig
    freetype
    fuse3
    gdk-pixbuf
    glib
    gtk3
    icu
    libGL
    libappindicator-gtk3
    libdrm
    libglvnd
    libnotify
    libpulseaudio
    libunwind
    libusb1
    libuuid
    libxkbcommon
    libxml2
    mesa
    nspr
    nss
    openssl
    pango
    pipewire
    stdenv.cc.cc
    systemd
    zlib
  ];
  nix-ld = pkgs.nix-ld-rs;
in {
  # Enable nix ld
  programs.nix-ld.enable = true;
  programs.nix-ld.package = nix-ld;

  programs.nix-ld.libraries = options.programs.nix-ld.libraries.default ++ libs;

  environment.systemPackages = [
    nix-ld
  ];
}
