{
  config,
  pkgs,
  inputs,
  lib,
  ...
}: {
  environment.binsh = "${pkgs.dash}/bin/dash";
  environment.systemPackages =
    (with pkgs; [
      atool
      fd
      jq
      libqalculate
      lzop
      man-pages
      magic-wormhole-rs
      mediainfo
      p7zip
      ripgrep
      unrar
      unzip
      sc-im
    ])
    ++ (with pkgs.unstable; [
      ]);

  # Set your time zone.
  time.timeZone = "Asia/Jerusalem";

  # automatically building the immutable cache
  documentation.man.generateCaches = true;

  # Optimization settings and garbage collection automation
  nix = {
    package = lib.mkDefault pkgs.nix;
    settings = {
      auto-optimise-store = true;
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
  };
}
