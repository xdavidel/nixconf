{
  config,
  pkgs,
  inputs,
  lib,
  ...
}: let
  user = lib.toLower config.mNixOS.userName;
in {
  users.users.${user}.extraGroups = ["wireshark"];

  programs.wireshark.enable = true;

  environment.systemPackages = with pkgs; [
    binwalk
    (cutter.withPlugins (ps: with ps; [jsdec rz-ghidra sigdb]))
    (rizin.withPlugins (ps: with ps; [rz-ghidra]))
    imhex
    wireshark
    zap
    nmap
    dig
    ghidra
    lurk # colorful strace
    inputs.self.packages.${pkgs.system}.binrev
  ];
}
