{
  lib,
  config,
  inputs,
  outputs,
  mLib,
  pkgs,
  ...
}: let
  cfg = config.mNixOS;
in {
  options.mNixOS = {
    userName = lib.mkOption {
      default = "User";
      description = ''
        username
      '';
    };

    userConfig = lib.mkOption {
      default = {};
      description = ''
        home-manager config path
      '';
    };

    userNixosSettings = lib.mkOption {
      default = {};
      description = ''
        NixOS user settings
      '';
    };
  };

  config = let
    user = lib.toLower cfg.userName;
  in {
    home-manager = {
      useGlobalPkgs = true;
      useUserPackages = true;

      extraSpecialArgs = {
        inherit inputs;
        inherit mLib;
        outputs = inputs.self.outputs;
      };
      users = {
        ${user} = {...}: {
          imports = [
            (import cfg.userConfig)
            outputs.homeManagerModules.default
            inputs.nix-colors.homeManagerModules.default
          ];
        };
      };
    };

    # Define a user account. Don't forget to set a password with ‘passwd’.
    users.users.${user} = {
      isNormalUser = true;
      description = "${cfg.userName}";
      extraGroups = ["wheel" "input"];
      packages = with pkgs; [
        home-manager
      ];

      # Allow the graphical user to login without password
      initialHashedPassword = "";
    };

    users.users.root.initialHashedPassword = "$y$j9T$TlPWaCGVEHGGXSwWBDLar0$lOL1x.VeMR3RGrPHfEzwxaOLmknfv3le6ZuunRIBnI3";
  };
}
