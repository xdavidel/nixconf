{
  config,
  lib,
  pkgs,
  inputs,
  ...
}: let
  awk = "${pkgs.gawk}/bin/awk";
  head = "${pkgs.coreutils}/bin/head";
  notify-send = "${pkgs.libnotify}/bin/notify-send";
  ps = "${pkgs.procps}/bin/ps";
  sort = "${pkgs.coreutils}/bin/sort";
  dmc = "${inputs.self.packages.${pkgs.system}.dmc}/bin/dmc";
  mem_hogs = ''
    ${ps} axch -o cmd:15,rss | \
        ${awk} '{a[$1] += $2} END {for (i in a) if (a[i] > 1024) {print i,  a[i]/1024"MB"}}' | \
        ${sort} -rnk2,2 | \
        ${head}
  '';
  cpu_hogs = ''
    ${ps} axch -o cmd:15,%cpu | \
    ${awk} '{a[$1] += $2} END{for (i in a) print i, a[i]"%"}' | \
    ${sort} -rnk2,2 | \
    ${head}
  '';
  inherit (config.colorscheme) colors;
in {
  programs.waybar = {
    enable = true;
    systemd.enable = true;
    settings = {
      topBar = let
        random-icon = "";
        repeat-icon = "";
        play-icon = "<span color='#${colors.base0B}'> </span>";
        pause-icon = "<span color='#${colors.base09}'> </span>";
      in {
        layer = "top";
        position = "top";
        height = 25;
        modules-center = [];
        modules-right = ["mpris" "memory" "cpu" "wireplumber" "battery"];

        "mpd" = {
          "format" = "{stateIcon}{consumeIcon}{randomIcon}{repeatIcon} {artist} - {title}";
          # "format-paused" = "{stateIcon} {artist} - {title}";
          "format-stopped" = "";
          "format-disconnected" = "";
          "artist-len" = 10;
          "title-len" = 35;

          "state-icons" = {
            "playing" = "${play-icon}";
            "paused" = "${pause-icon}";
          };
          "random-icons" = {
            "off" = "<span color='#${colors.base04}'>${random-icon}</span> "; # Icon grayed out when "random" is off
            "on" = "<span color='#${colors.base06}'>${random-icon}</span> "; # Icon grayed out when "random" is off
          };
          "repeat-icons" = {
            "off" = "<span color='#${colors.base04}'>${repeat-icon}</span> "; # Icon grayed out when "random" is off
            "on" = "<span color='#${colors.base06}'>${repeat-icon}</span> "; # Icon grayed out when "random" is off
          };
          "consume-icons" = {
            "on" = " "; # Icon shows only when "consume" is on
          };
        };

        "mpris" = {
          # "ignored-players" = ["mpd"];
          "format" = "{player_icon} {dynamic}";
          "format-paused" = "{status_icon} {dynamic}";
          "dynamic-len" = 100;
          "dynamic-order" = ["title" "artist" "album" "length"];
          # "dynamic-order" = ["title" "artist" "album" ""position" "length"];
          # "interval" = 1;
          "player-icons" = {
            "default" = "${play-icon}";
            "mpv" = "<span color='#${colors.base0B}'> </span>";
            "brave" = "🦁";
            "firefox" = "🦊";
          };
          "status-icons" = {
            "paused" = "${pause-icon}";
          };

          on-click-right = "${pkgs.playerctl}/bin/playerctld shift";
          on-click-middle = "${pkgs.playerctl}/bin/playerctld unshift";
        };

        "memory" = {
          "interval" = 30;
          "format" = "{used:0.1f}G/{total:0.1f}G 🧠";
          "on-click" = "${notify-send} \"Memory Hogs\" \"$(${mem_hogs})\"";
        };

        "cpu" = {
          "interval" = 10;
          "format" = "{usage}% {icon0}{icon1}{icon2}{icon3}{icon4}{icon5}{icon6}{icon7}";
          "format-icons" = [
            "<span color='#${colors.base0B}'>▁</span>" # green
            "<span color='#${colors.base0D}'>▂</span>" # blue
            "<span color='#${colors.base06}'>▃</span>" # white
            "<span color='#${colors.base06}'>▄</span>" # white
            "<span color='#${colors.base0A}'>▅</span>" # yellow
            "<span color='#${colors.base0A}'>▆</span>" # yellow
            "<span color='#${colors.base09}'>▇</span>" # orange
            "<span color='#${colors.base08}'>█</span>" # red
          ];
          "on-click" = "${notify-send} \"CPU Hogs\" \"$(${cpu_hogs})\"";
        };
        "wireplumber" = {
          "format" = "{volume}% {icon}";
          "format-muted" = "🔇";
          "on-click" = "${dmc} -c";
          "on-click-middle" = "${dmc} -m";
          "format-icons" = ["🔈" "🔉" "🔊"];
          "max-volume" = "150.0";
        };
        "battery" = {
          "interval" = 60;
          "states" = {
            "warning" = 30;
            "critical" = 25;
          };
          "format" = "{capacity}% {icon}";
          "format-full" = "{capacity}% ⚡";
          # "format-discharging" = "{capacity}% 🔋";
          "format-charging" = "{capacity}% 🔌";
          # "format-icons" = ["🔋"];
          "format-icons" = [
            "<span color='#${colors.base08}'> </span>"
            "<span color='#${colors.base09}'> </span>"
            "<span color='#${colors.base0A}'> </span>"
            "<span color='#${colors.base0B}'> </span>"
            "<span color='#${colors.base0B}'> </span>"
          ];
        };
      };
      bottomBar = {
        layer = "top";
        position = "bottom";
        height = 25;
        modules-left = ["wlr/taskbar"];
        modules-right = ["clock" "tray"];
        "clock" = {
          format = "{:%Y/%m/%d %H:%M} 🕖";
          format-alt = "{:%Y %B %d (%A) %R} 🕖";
          tooltip-format = "<tt><small>{calendar}</small></tt>";
          calendar = {
            mode = "year";
            mode-mon-col = 3;
            weeks-pos = "right";
            on-scroll = 1;
            on-click-right = "mode";
            format = {
              months = "<span color='#${colors.base0A}'><b>{}</b></span>";
              days = "<span color='#${colors.base06}'><b>{}</b></span>";
              weeks = "<span color='#${colors.base0B}'><b>W{}</b></span>";
              weekdays = "<span color='#${colors.base09}'><b>{}</b></span>";
              today = "<span color='#${colors.base08}'><b><u>{}</u></b></span>";
            };
          };
        };
      };
    };
    style =
      /*
      css
      */
      ''
        * {
          border: none;
          border-radius: 0;
          font-family: 'Cascadia Code', 'SFNS Display',  Helvetica, Arial, sans-serif;
          font-size: 12px;
          min-height: 0;
          margin: 0;
          padding: 0;
          color: #ffffff;
        }

        window#waybar {
          background: rgba(43, 48, 59, 0.5);
        }

        window#waybar.hidden {
          opacity: 0.2;
        }

        #window.focused {
          padding: 0 20px;
          margin: 3px 0;
          font-weight: bold;
          border-radius: 10px;
          min-width: 300px;
          background: #75507b;
        }

        #workspaces button {
          padding: 2px 5px;
        }

        #workspaces button.active {
          background: #75507b;
          border-bottom: 3px solid #ffffff;
        }

        #workspaces button.urgent {
          background-color: #eb4d4b;
        }
        #workspaces button:hover {
          box-shadow: 0 0 2px #7550ff;
          background: #75509f;
          transition-property: background;
          transition-duration: 0.8s;
        }

        #taskbar button{
          border-top: 3px solid transparent;
        }
        #taskbar button.active{
          border-top: 3px solid #ffffff;
        }

        #taskbar button:hover{
          box-shadow: 0 0 2px #7550ff;
          background: #75509f;
          transition-property: background;
          transition-duration: 0.8s;
        }

        tooltip {
          background: #1e1e2e;
          border-radius: 10px;
          border-width: 2px;
          border-style: solid;
          border-color: #11111b;
        }

        #submap,
        #clock,
        #cpu,
        #mpd,
        #mpris,
        #wireplumber,
        #battery,
        #tray,
        #language,
        #memory {
          padding: 0 5px;
          margin-left: 5px;
          background: rgba(43, 48, 59, 0.5);
          color: #fefefe;
        }
      '';
  };
}
