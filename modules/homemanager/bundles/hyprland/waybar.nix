{
  config,
  lib,
  pkgs,
  inputs,
  ...
}: {
  programs.waybar.settings = lib.mkIf (config.mHomeManager.bundles.hyprland.enable) {
    topBar.modules-left = ["hyprland/workspaces" "hyprland/submap"];
    bottomBar.modules-center = ["hyprland/window"];
    bottomBar.modules-right = lib.mkBefore ["hyprland/language"];
    bottomBar."hyprland/language" = {
      on-click = "hyprctl switchxkblayout at-translated-set-2-keyboard next";
      format = "{} 📖";
      format-en = "En";
      format-he = "He";
    };
    bottomBar."wlr/taskbar" = {
      format = "{icon}";
      icon-size = 20;
      on-click = "minimize-raise";
    };
  };
}
