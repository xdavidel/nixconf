{
  lib,
  config,
  pkgs,
  inputs,
  ...
} @ args: let
  autorun = ''[ -z "$DISPLAY" ] && [ "$XDG_VTNR" = 1 ] && exec Hyprland'';
  gaps = 5;
  dmc = "${inputs.self.packages.${pkgs.system}.dmc}/bin/dmc";
in {
  imports = [
    ./waybar.nix
  ];

  mHomeManager.bundles = {
    wayland.enable = true;
    waybar.enable = true;
  };

  services.hypridle = let
    locker = "${pkgs.hyprlock}/bin/hyprlock";
  in {
    enable = true;
    settings = {
      general = {
        after_sleep_cmd = "${inputs.self.packages.${pkgs.system}.hyprtools}/bin/hyprmon -a";
        before_sleep_cmd = "${pkgs.systemd}/bin/loginctl lock-session";
        ignore_dbus_inhibit = false;
        lock_cmd = "${locker}";
      };

      listener = [
        {
          timeout = 1200;
          on-timeout = "${pkgs.systemd}/bin/loginctl lock-session";
        }
        {
          timeout = 600;
          on-timeout = "hyprctl dispatch dpms off";
          on-resume = "hyprctl dispatch dpms on";
        }
      ];
    };
  };

  programs.hyprlock = {
    enable = true;
    settings = {
      general = {
        disable_loading_bar = true;
        hide_cursor = true;
        no_fade_in = false;
      };

      background = [
        {
          path = "screenshot";
          blur_passes = 3;
          blur_size = 8;
        }
      ];
      label = [
        {
          monitor = "";
          text = "$TIME";
          text_align = "center";
          color = "rgba(200, 200, 200, 1.0)";
          font_size = 15;
          rotate = 0; # degrees, counter-clockwise
          position = "0, 110";
          halign = "center";
          valign = "center";
        }
        {
          monitor = "";
          text = ''Hi there, $USER'';
          text_align = "center";
          color = "rgba(200, 200, 200, 1.0)";
          font_size = 25;
          rotate = 0; # degrees, counter-clockwise
          position = "0, 60";
          halign = "center";
          valign = "center";
        }
      ];

      input-field = [
        {
          size = "200, 50";
          position = "0, -70";
          monitor = "";
          dots_center = true;
          fade_on_empty = false;
          font_color = "rgb(202, 211, 245)";
          inner_color = "rgb(91, 96, 120)";
          outer_color = "rgb(24, 25, 38)";
          outline_thickness = 4;
          placeholder_text = "Password...";
          shadow_passes = 2;
        }
      ];
    };
  };

  wayland.windowManager.hyprland = let
    wpctl = "${pkgs.wireplumber}/bin/wpctl";
  in {
    enable = true;
    xwayland.enable = true;
    package = lib.mkIf (builtins.hasAttr "osConfig" args) args.osConfig.programs.hyprland.package;
    systemd = {
      enable = true;
      # Same as default, but stop graphical-session too
      extraCommands = lib.mkBefore [
        "systemctl --user stop graphical-session.target"
        "systemctl --user start hyprland-session.target"
      ];
    };

    settings = let
      inherit (config.colorscheme) colors;
    in {
      exec-once = "${inputs.self.packages.${pkgs.system}.autostart}/bin/autostart";
      general = {
        gaps_in = gaps;
        gaps_out = gaps;
        border_size = 2;

        "col.active_border" = "rgba(${colors.base08}ff)";
        "col.inactive_border" = "rgba(${colors.base01}cc)";

        layout = "master";
      };
      cursor = {
        inactive_timeout = 10;
        hide_on_touch = true;
        hide_on_key_press = true;
      };
      input = {
        kb_layout = "us,il";
        kb_options = "grp:alt_shift_toggle,caps:escape";
        numlock_by_default = true;
        touchpad.disable_while_typing = true;

        repeat_delay = 300;
        repeat_rate = 50;
      };
      dwindle = {
        force_split = 2; # split on right and bottom
      };

      master = {
        new_status = "master";
        new_on_top = true;
      };

      layerrule = [
        "blur,waybar"
        "ignorezero,waybar"
      ];

      decoration = {
        # active_opacity = 1.0;
        # inactive_opacity = 0.75;
        fullscreen_opacity = 1.0;
        rounding = 1;
        blur = {
          enabled = false;
        };
        shadow = {
          enabled = false;
        };
      };
      animations = {
        enabled = true;
      };

      misc = {
        vfr = true;
        disable_hyprland_logo = false;
        disable_splash_rendering = true;
        mouse_move_enables_dpms = true;
        key_press_enables_dpms = true;
        new_window_takes_over_fullscreen = 2;
      };

      binds = {
        allow_workspace_cycles = true;
      };

      "$mod" = "SUPER";

      # mouse bindings
      bindm = [
        "SUPER,mouse:272,movewindow"
        "SUPER,mouse:273,resizewindow"
      ];

      # repeatalbe bindings
      binde = let
        brightnessctl = "${pkgs.brightnessctl}/bin/brightnessctl";
        resizes = rec {
          left = "-50 0";
          right = "50 0";
          up = "0 -40";
          down = "0 40";
          h = left;
          l = right;
          k = up;
          j = down;
        };
      in
        [
          ",XF86MonBrightnessDown,exec,${brightnessctl} s 5%-"
          ",XF86MonBrightnessUp,exec,${brightnessctl} s 5%+"
          ",XF86Eject,exec,eject -T"
          ",XF86AudioMicMute,exec,pactl set-source-mute @DEFAULT_SOURCE@ toggle"
          ",XF86AudioRaiseVolume,exec, ${wpctl} set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 1%+; "
          ",XF86AudioLowerVolume,exec, ${wpctl} set-volume @DEFAULT_AUDIO_SINK@ 2%-; "
          "$mod,equal,exec, ${wpctl} set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 1%+; "
          "$mod,minus,exec, ${wpctl} set-volume @DEFAULT_AUDIO_SINK@ 2%-; "

          "$mod,bracketright,exec,${dmc} -f 5"
          "$mod,bracketleft,exec,${dmc} -b 5"
          "$mod SHIFT,bracketright,exec,${dmc} -f 10"
          "$mod SHIFT,bracketleft,exec,${dmc} -b 10"
        ]
        ++
        # Resize windows
        (lib.mapAttrsToList (
            key: resize: "$mod ALT,${key},resizeactive,${resize}"
          )
          resizes);

      # lock enabled bindings
      bindl = [
        ",switch:on:Lid Switch,exec,${inputs.self.packages.${pkgs.system}.hyprtools}/bin/hyprmon -a"
        ",switch:off:Lid Switch,exec,${inputs.self.packages.${pkgs.system}.hyprtools}/bin/hyprmon -a"
        "$mod SHIFT,escape,exec,hyprctl reload && ${pkgs.libnotify}/bin/notify-send 'Hyprland' 'Configration Reloaded' &"
        "$mod,BackSpace,exec,${inputs.self.packages.${pkgs.system}.hyprtools}/bin/hyprmon -a"
        ",XF86AudioPause,exec,${dmc} -S"
        ",XF86AudioPlay,exec,${dmc} -P"
        ",XF86AudioPrev,exec,${dmc} -p"
        ",XF86AudioNext,exec,${dmc} -n"
        ",XF86AudioMute,exec,${wpctl} set-mute @DEFAULT_AUDIO_SINK@ toggle"
        "$mod,m,exec,${wpctl} set-mute @DEFAULT_AUDIO_SINK@ toggle"
        "$mod,p,exec,${dmc} -P"
        "$mod SHIFT,p,exec,${pkgs.playerctl}/bin/playerctl play-pause"
        "$mod CTRL SHIFT,p,exec,${pkgs.playerctl}/bin/playerctl -a pause"

        "$mod,comma,exec,${dmc} -p"
        "$mod,period,exec,${dmc} -n"
        "$mod SHIFT,comma,exec,${pkgs.playerctl}/bin/playerctld shift"
        "$mod SHIFT,period,exec,${pkgs.playerctl}/bin/playerctld unshift"
      ];

      bind = let
        pkill = "${pkgs.procps}/bin/pkill";
        # Map keys (arrows and hjkl) to hyprland directions (l, r, u, d)
        directions = rec {
          left = "l";
          right = "r";
          up = "u";
          down = "d";
          h = left;
          l = right;
          k = up;
          j = down;
        };
        master_nav = rec {
          up = "cycleprev";
          down = "cyclenext";
          k = up;
          j = down;
        };
        master_swap = rec {
          up = "swapprev";
          down = "swapnext";
          k = up;
          j = down;
        };
        window_move_monitor = rec {
          left = "mon:l";
          right = "mon:r";
          h = left;
          l = right;
        };
        workspace_nav = rec {
          left = "m-1";
          right = "m+1";
          h = left;
          l = right;
        };
        terminal = config.home.sessionVariables.TERMINAL;
      in
        [
          # Program bindings
          "$mod,Return,exec,${terminal}"
          "$mod,slash,exec,${pkgs.bemenu}/bin/bemenu-run -w --fn 'mono 14'"
          "$mod,q,killactive"
          "$mod,x,exec,${inputs.self.packages.${pkgs.system}.sysact}/bin/sysact -l"
          "$mod,escape,exec,${inputs.self.packages.${pkgs.system}.sysact}/bin/sysact"
          "$mod,c,exec,${inputs.self.packages.${pkgs.system}.nmctl}/bin/nmctl -c"
          "$mod,e,exec,${pkgs.xfce.thunar}/bin/thunar"
          "$mod,b,exec,${pkill} -SIGUSR1 waybar"
          "$mod SHIFT,b,exec,systemctl --user restart waybar"
          "$mod SHIFT,e,exit"
          "$mod,Print,exec,${inputs.self.packages.${pkgs.system}.hyprtools}/bin/screenshot"
          ",Print,exec,${inputs.self.packages.${pkgs.system}.hyprtools}/bin/screenshot -f"
          "CTRL,Print,exec,${inputs.self.packages.${pkgs.system}.hyprtools}/bin/screenshot -a"
          "ALT,Print,exec,${inputs.self.packages.${pkgs.system}.hyprtools}/bin/screenshot -w"

          "$mod,semicolon,exec,${inputs.self.packages.${pkgs.system}.iconsel}/bin/iconsel -e"
          "$mod SHIFT,semicolon,exec,${inputs.self.packages.${pkgs.system}.iconsel}/bin/iconsel -a"

          "$mod,f,fullscreen,1"
          "$mod,y,pin,active"
          "$mod SHIFT,f,togglefloating"
          "$mod CONTROL,f,exec,${inputs.self.packages.${pkgs.system}.hyprtools}/bin/togglefloatfocus"
          "$mod,Tab,workspace, previous"
          "ALT,Tab,focuscurrentorlast"
          "$mod CONTROL,up,cyclenext,prev"
          "$mod CONTROL,down,cyclenext"
          "$mod CONTROL,k,cyclenext,prev"
          "$mod CONTROL,j,cyclenext"
          "$mod SHIFT,delete,exec,hyprctl kill"
          "$mod ALT,minus,movetoworkspace, special:SP"
          "$mod ALT,return,togglespecialworkspace, SP"

          "$mod SHIFT,m,exec, ${terminal} -e ${pkgs.ncmpcpp}/bin/ncmpcpp"

          "$mod,space,layoutmsg,swapwithmaster"
          "$mod,i,layoutmsg,addmaster"
          "$mod SHIFT,i,layoutmsg,removemaster"
          "$mod,t,layoutmsg,orientationleft"
          "$mod SHIFT,t,layoutmsg,orientationtop"
          "$mod,g,layoutmsg,orientationcenter"

          "$mod,u,togglegroup"
          "$mod SHIFT,u,changegroupactive"

          "$mod ALT,BackSpace,exec,${inputs.self.packages.${pkgs.system}.hyprtools}/bin/hyprmon -e"
          "$mod SHIFT,BackSpace,exec,${inputs.self.packages.${pkgs.system}.hyprtools}/bin/hyprmon -w"
          "$mod CTRL,BackSpace,exec,${inputs.self.packages.${pkgs.system}.hyprtools}/bin/hyprmon -m"
        ]
        ++ (lib.mapAttrsToList (
            key: direction: "$mod,${key},layoutmsg,${direction}"
          )
          master_nav)
        ++
        # Swap windows
        (lib.mapAttrsToList (
            key: direction: "$mod SHIFT,${key},layoutmsg,${direction}"
          )
          master_swap)
        ++
        # move windows to monitor
        (lib.mapAttrsToList (
            key: direction: "$mod SHIFT,${key},movewindow,${direction}"
          )
          window_move_monitor)
        ++ (lib.mapAttrsToList (
            key: direction: "$mod CONTROL,${key},workspace,${direction}"
          )
          workspace_nav)
        ++ (lib.mapAttrsToList (
            key: direction: "$CONTROL ALT,${key},movewindoworgroup,${direction}"
          )
          directions)
        ++
        # Move monitor focus
        (lib.mapAttrsToList (
            key: direction: "$mod,${key},focusmonitor,${direction}"
          )
          directions);
    };

    # This is order sensitive, so it has to come here.
    extraConfig = ''
      env = XDG_CURRENT_DESKTOP, Hyprland
      env = XDG_SESSION_DESKTOP, Hyprland

      env = CLUTTER_BACKEND, wayland
      env = GDK_BACKEND, wayland, x11
      env = MOZ_ENABLE_WAYLAND, 1
      env = NIXOS_OZONE_WL, 1
      env = QT_QPA_PLATFORM=wayland;xcb
      env = QT_WAYLAND_DISABLE_WINDOWDECORATION, 1
      env = QT_AUTO_SCREEN_SCALE_FACTOR, 1
      env = WLR_NO_HARDWARE_CURSORS, 1
      env = XDG_SESSION_TYPE, wayland
      env = SDL_VIDEODRIVER, x11

      windowrulev2 = tile, class:^(ghidra-Ghidra)$,title:^(CodeBrowser.*)$
      # windowrulev2 = noinitialfocus, class:^(ghidra-Ghidra)$,title:^(win.+)$
      # windowrulev2 = nofocus, class:^(ghidra-Ghidra)$,title:^ $
      # windowrulev2 = windowdance, class:^(ghidra-Ghidra)$,floating:1

      windowrulev2 = noinitialfocus, class:^(sourceinsight4.exe)$,title:^(win.+)$
      windowrulev2 = nofocus, class:^(sourceinsight4.exe)$,title:^ $
      #windowrulev2 = windowdance, class:^(sourceinsight4.exe)$,floating:1

      windowrulev2 = bordercolor rgb(FFFF00),pinned:1
      windowrulev2 = idleinhibit always, class:^(mpv)$

      windowrulev2 = float, class:^(file-roller)

      # smart gaps
      workspace = w[t1], gapsout:0, gapsin:0
      workspace = f[1], gapsout:0, gapsin:0
      windowrulev2 = bordersize 0, floating:0, onworkspace:w[t1]
      windowrulev2 = rounding 0, floating:0, onworkspace:w[t1]
      windowrulev2 = bordersize 0, floating:0, onworkspace:f[1]
      windowrulev2 = rounding 0, floating:0, onworkspace:f[1]

      bind = $mod, 1, workspace, 1
      bind = $mod, 2, workspace, 2
      bind = $mod, 3, workspace, 3
      bind = $mod, 4, workspace, 4
      bind = $mod, 5, workspace, 5
      bind = $mod, 6, workspace, 6
      bind = $mod, 7, workspace, 7
      bind = $mod, 8, workspace, 8
      bind = $mod, 9, workspace, 9
      bind = $mod, 0, workspace, 10
      bind = $mod, F1, workspace, 11
      bind = $mod, F2, workspace, 12
      bind = $mod, F3, workspace, 13
      bind = $mod, F4, workspace, 14
      bind = $mod, F5, workspace, 15
      bind = $mod, F6, workspace, 16
      bind = $mod, F7, workspace, 17
      bind = $mod, F8, workspace, 18
      bind = $mod, F9, workspace, 19

      bind = $mod SHIFT, 1, movetoworkspacesilent, 1
      bind = $mod SHIFT, 2, movetoworkspacesilent, 2
      bind = $mod SHIFT, 3, movetoworkspacesilent, 3
      bind = $mod SHIFT, 4, movetoworkspacesilent, 4
      bind = $mod SHIFT, 5, movetoworkspacesilent, 5
      bind = $mod SHIFT, 6, movetoworkspacesilent, 6
      bind = $mod SHIFT, 7, movetoworkspacesilent, 7
      bind = $mod SHIFT, 8, movetoworkspacesilent, 8
      bind = $mod SHIFT, 9, movetoworkspacesilent, 9
      bind = $mod SHIFT, 0, movetoworkspacesilent, 10
      bind = $mod SHIFT, F1, movetoworkspacesilent, 11
      bind = $mod SHIFT, F2, movetoworkspacesilent, 12
      bind = $mod SHIFT, F3, movetoworkspacesilent, 13
      bind = $mod SHIFT, F4, movetoworkspacesilent, 14
      bind = $mod SHIFT, F5, movetoworkspacesilent, 15
      bind = $mod SHIFT, F6, movetoworkspacesilent, 16
      bind = $mod SHIFT, F7, movetoworkspacesilent, 17
      bind = $mod SHIFT, F8, movetoworkspacesilent, 18
      bind = $mod SHIFT, F9, movetoworkspacesilent, 19


      bind = $mod CONTROL, 1, focusworkspaceoncurrentmonitor, 1
      bind = $mod CONTROL, 2, focusworkspaceoncurrentmonitor, 2
      bind = $mod CONTROL, 3, focusworkspaceoncurrentmonitor, 3
      bind = $mod CONTROL, 4, focusworkspaceoncurrentmonitor, 4
      bind = $mod CONTROL, 5, focusworkspaceoncurrentmonitor, 5
      bind = $mod CONTROL, 6, focusworkspaceoncurrentmonitor, 6
      bind = $mod CONTROL, 7, focusworkspaceoncurrentmonitor, 7
      bind = $mod CONTROL, 8, focusworkspaceoncurrentmonitor, 8
      bind = $mod CONTROL, 9, focusworkspaceoncurrentmonitor, 9
      bind = $mod CONTROL, 0, focusworkspaceoncurrentmonitor, 10
      bind = $mod CONTROL, F1, focusworkspaceoncurrentmonitor, 11
      bind = $mod CONTROL, F2, focusworkspaceoncurrentmonitor, 12
      bind = $mod CONTROL, F3, focusworkspaceoncurrentmonitor, 13
      bind = $mod CONTROL, F4, focusworkspaceoncurrentmonitor, 14
      bind = $mod CONTROL, F5, focusworkspaceoncurrentmonitor, 15
      bind = $mod CONTROL, F6, focusworkspaceoncurrentmonitor, 16
      bind = $mod CONTROL, F7, focusworkspaceoncurrentmonitor, 17
      bind = $mod CONTROL, F8, focusworkspaceoncurrentmonitor, 18
      bind = $mod CONTROL, F9, focusworkspaceoncurrentmonitor, 19


      bind = $mod,r,submap, floatctl
      submap=floatctl

      # move floating window
      binde = , right,     moveactive,       30 0
      binde = , left,      moveactive,      -30 0
      binde = , up,        moveactive,       0 -30
      binde = , down,      moveactive,       0 30

      binde = ALT,    right,     resizeactive,     50 0
      binde = ALT,    left,      resizeactive,    -50 0
      binde = ALT,    up,        resizeactive,     0 -40
      binde = ALT,    down,      resizeactive,     0 40

      bind=$mod,r,submap,reset
      bind=,escape,submap,reset
      submap=reset

      bind = $mod, F10, submap, passthrough
      submap=passthrough
      bind=,escape,submap,reset
      bind = $mod, F10, submap, reset
      submap=reset
    '';
  };
}
