{
  config,
  lib,
  pkgs,
  inputs,
  ...
} @ args: {
  dconf.settings = {
    "org/gnome/desktop/interface" = {
      color-scheme = "prefer-dark";
    };
  };

  services.blueman-applet.enable = lib.mkIf (builtins.hasAttr "osConfig" args) args.osConfig.services.blueman.enable;

  graphical.option = true;

  home.packages = with pkgs; [
    telegram-desktop
  ];
}
