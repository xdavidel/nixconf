{
  lib,
  config,
  pkgs,
  inputs,
  ...
}: let
  autorun = ''[ -z "$DISPLAY" ] && [ "$XDG_VTNR" = 1 ] && exec river'';
  gaps = "10";
in {
  imports = [
    ./waybar.nix
  ];

  mHomeManager.bundles = {
    wayland.enable = true;
    waybar.enable = true;
  };

  programs.bash.profileExtra = "${autorun}";
  programs.zsh.profileExtra = "${autorun}";

  programs.foot = {
    enable = true;
    server.enable = true;
  };

  wayland.windowManager.river = {
    enable = true;
    xwayland.enable = true;
    systemd = {
      enable = true;
      extraCommands = lib.mkBefore [
        "systemctl --user stop river-session.target"
        "systemctl --user start river-session.target"
      ];
    };

    settings = {
      border-width = 3;
      map = {
        normal = {
          "Super Return" = ''spawn ${pkgs.foot}/bin/footclient'';
          "Super+Shift E" = "exit";
          "Super Q" = "close";
          "Super slash" = ''spawn "${pkgs.bemenu}/bin/bemenu-run -w --fn 'mono 14'"'';

          "Super+Shift F" = "toggle-float";

          "Super Escape" = "focus-previous-tags";
          "Super Tab" = "focus-previous-tags";
          "Alt Tab" = "focus-previous-tags";

          # Super+J and Super+K to focus the next/previous view in the layout stack
          "Super J" = "focus-view next";
          "Super K" = "focus-view previous";
          "Super down" = "focus-view next";
          "Super up" = "focus-view previous";

          # Super+Shift+J and Super+Shift+K to swap the focused view with the next/previous
          # view in the layout stack
          "Super+Shift J" = "swap next";
          "Super+Shift K" = "swap previous";
          "Super+Shift down" = "swap next";
          "Super+Shift up" = "swap previous";

          # Super+Period and Super+Comma to focus the next/previous output
          "Super right" = "focus-output next";
          "Super left" = "focus-output previous";

          # Super+Shift+{Period,Comma} to send the focused view to the next/previous output
          "Super+Shift right" = "send-to-output next";
          "Super+Shift left" = "send-to-output previous";

          # Super+Return to bump the focused view to the top of the layout stack
          "Super space" = "zoom";

          # Toggle waybar
          "Super b" = ''spawn "pkill -SIGUSR1 '^waybar$'"'';

          # Refresh waybar
          "Super+Shift b" = ''spawn "pkill -SIGUSR2 '^waybar$'"'';

          # Super+Space to toggle float
          "Super+Shift f" = "toggle-float";

          # Super+F to toggle fullscreen
          "Super F" = "toggle-fullscreen";

          # Super + Left Mouse Button to move views
          "Super BTN_LEFT" = "move-view";

          # Super + Right Mouse Button to resize views
          "Super BTN_RIGHT" = "resize-view";
        };
      };
      set-repeat = "50 300";
    };

    extraConfig =
      /*
      bash
      */
      ''
              riverctl background-color 0x002b36
              riverctl border-color-focused 0xd36265
              riverctl border-color-unfocused 0x586e75

        # Mouse configurations
              riverctl focus-follows-cursor normal
              riverctl hide-cursor timeout 10000
              riverctl hide-cursor when-typing enabled
              riverctl set-cursor-warp on-output-change

        # Set the keyboard layout options
              riverctl keyboard-layout -options "grp:alt_shift_toggle,caps:escape" 'us,il'

        # Add new Clients to the bottom of the stack
              riverctl attach-mode bottom

        # Make certain views start floating
              riverctl float-filter-add app-id float
              riverctl float-filter-add title "popup title with spaces 'Media viewer'"
              riverctl float-filter-add title "File Operation Progress"
              riverctl float-filter-add title "Confirm to replace files"

        # Set app-ids and titles of views which should use client side decorations
              riverctl csd-filter-add app-id "gedit"

        # set touchpad
              for pad in `riverctl list-inputs | grep -i touchpad`; do
                riverctl input $pad events enabled
                  riverctl input $pad tap enabled
                  done

        ## TAGS
        ## ======================================================

                      for i in `seq 1 9`; do
                        tags=$((1 << ($i - 1)))

        # Super+[1-9] to focus tag [0-8]
                          riverctl map -layout 0 normal Super $i set-focused-tags $tags

        # Super+Shift+[1-9] to tag focused view with tag [0-8]
                          riverctl map -layout 0 normal Super+Shift $i set-view-tags $tags

        # Super+Ctrl+[1-9] to toggle focus of tag [0-8]
                          riverctl map -layout 0 normal Super+Control $i toggle-focused-tags $tags

        # Super+Shift+Ctrl+[1-9] to toggle tag [0-8] of focused view
                          riverctl map -layout 0 normal Super+Shift+Control $i toggle-view-tags $tags
                          done

        # Super+0 to focus all tags
        # Super+Shift+0 to tag focused view with all tags
                          all_tags=$(((1 << 32) - 1))
                          riverctl map -layout 0 normal Super 0 set-focused-tags $all_tags
                          riverctl map -layout 0 normal Super+Shift 0 set-view-tags $all_tags

        # scrachpads will live on an unused tag.
                          scratch_tag="$((1 << 20 ))"

        # toggle scrachpad
                          riverctl map -layout 0 normal Super+Alt Return toggle-focused-tags $scratch_tag

        # send window to scratch_tag
            riverctl map -layout 0 normal Super+Alt minus set-view-tags $scratch_tag

        # make a window sticky
            riverctl map -layout 0 normal Super y set-view-tags "$all_tags"

        # set spawn tagmask to ensure new windows don't have the scrachpad tag
              all_but_scratch_tag=$(( ((1 << 32) -1) ^ $scratch_tag ))
              riverctl spawn-tagmask $all_but_scratch_tag

              riverctl default-layout rivertile
              riverctl spawn "rivertile -view-padding ${gaps} -outer-padding ${gaps} -main-ratio 0.55"
      '';
  };
}
