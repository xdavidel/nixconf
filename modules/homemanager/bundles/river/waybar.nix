{
  config,
  lib,
  pkgs,
  inputs,
  ...
}: {
  programs.waybar = lib.mkIf (config.mHomeManager.bundles.river.enable) {
    settings = {
      topBar.modules-left = ["river/tags"];
      bottomBar.modules-center = ["river/window"];
      bottomBar."river/window" = {
        format = "{:.80}";
        tooltip = true;
      };
    };

    style =
      /*
      css
      */
      ''
        #tags button {
          /*       u   r   d   l */
          padding: 2px 8px 0px 8px;
          min-width: 0;
          border-bottom: 3px solid transparent;
        }
        #tags button.occupied {
          border-bottom: 3px solid #ffffff;
        }
        #tags button.focused {
        background: #75507b;
                    font-weight: bold;
        }
        #tags button.urgent {
          background-color: #eb4d4b;
        }
        #tags button:hover {
          box-shadow: 0 0 2px #7550ff;
        background: #75509f;
                    transition-property: background;
                    transition-duration: 0.8s;
        }
      '';
  };
}
