{
  config,
  lib,
  pkgs,
  ...
} @ args: {
  services.network-manager-applet.enable = lib.mkIf (builtins.hasAttr "osConfig" args) args.osConfig.networking.networkmanager.enable;
}
