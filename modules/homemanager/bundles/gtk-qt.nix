{
  pkgs,
  inputs,
  config,
  ...
}: let
  inherit (inputs.nix-colors.lib-contrib {inherit pkgs;}) gtkThemeFromScheme;
  gtk_theme = {
    name = "${config.colorScheme.slug}";
    pkg = gtkThemeFromScheme {scheme = config.colorScheme;};
  };
  cursor_theme = {
    name = "Numix-Cursor";
    pkg = pkgs.numix-cursor-theme;
  };
in {
  gtk = {
    enable = true;
    iconTheme = {
      name = "Papirus-Dark";
      package = pkgs.papirus-icon-theme;
    };

    theme = {
      name = gtk_theme.name;
      package = gtk_theme.pkg;
    };

    cursorTheme = {
      name = cursor_theme.name;
      package = cursor_theme.pkg;
    };

    gtk3.extraConfig = {
      gtk-application-prefer-dark-theme = true;
    };
  };

  qt = {
    enable = true;
    platformTheme.name = "gtk";
    style = {
      name = "adwaita-dark";
      package = pkgs.adwaita-qt;
    };
  };

  # fix cursor theme in places that does not respect it
  home.file.".local/share/icons/default/index.theme" = {
    text = ''
      [Icon Theme]
      Inherits=${cursor_theme.name}
    '';
    force = true;
  };

  home.sessionVariables.GTK_THEME = gtk_theme.name;
  home.sessionVariables.XCURSOR_THEME = cursor_theme.name;
}
