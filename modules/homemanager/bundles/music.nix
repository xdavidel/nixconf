{
  config,
  lib,
  pkgs,
  ...
}: let
  mpd-desktop-name = "mpd-desktop";
  music_script = "music-open";
  music_dir = "~/Music";
  open-with-mpd =
    pkgs.writeScriptBin "${music_script}"
    /*
    bash
    */
    ''
      MUSIC_DIR="$(realpath ${music_dir})"
      MUSIC_FILE="$1"

      FILE_PATH="$(realpath --relative-to="$MUSIC_DIR" "$MUSIC_FILE")"

      # try to open with mpd and use mpv as fallback
      ${pkgs.mpc-cli}/bin/mpc insert "$FILE_PATH" && \
        ${pkgs.mpc-cli}/bin/mpc next || \
        ${pkgs.mpv}/bin/mpv --force-window --osc --no-resume-playback "$FILE_PATH"
    '';
in {
  services.playerctld.enable = true;

  services.mpd = {
    enable = true;
    # network.port = 6600;
    network.startWhenNeeded = true;
    musicDirectory = "${music_dir}";
    extraConfig = ''
      audio_output {
        type "pipewire"
        name "Pipewire Playback"
      }
    '';
  };

  home.packages = let
    music-desktop = pkgs.makeDesktopItem {
      name = mpd-desktop-name;
      desktopName = "Music Player";
      genericName = "Music Player";
      exec = ''${open-with-mpd}/bin/${music_script} %u'';
      categories = ["Audio" "AudioVideo"];
      mimeTypes = [
        "audio/mpeg"
        "audio/wav"
        "audio/ogg"
      ];
    };
  in
    []
    ++ (
      if (config.graphical.option == true)
      then [music-desktop]
      else []
    );

  xdg.mimeApps.defaultApplications = let
    mpd = "${mpd-desktop-name}.desktop";
  in {
    "audio/mpeg" = "${mpd}";
  };

  services.mpd-mpris.enable = true;

  programs.ncmpcpp = {
    enable = true;
    bindings = [
      {
        key = "j";
        command = "scroll_down";
      }
      {
        key = "k";
        command = "scroll_up";
      }
      {
        key = "m";
        command = "move_sort_order_down";
      }
      {
        key = "M";
        command = "move_sort_order_up";
      }
      {
        key = "+";
        command = "show_clock";
      }
      {
        key = "=";
        command = "volume_up";
      }
      {
        key = "left";
        command = "previous_column";
      }
      {
        key = "h";
        command = "previous_column";
      }
      {
        key = "left";
        command = "jump_to_parent_directory";
      }
      {
        key = "h";
        command = "jump_to_parent_directory";
      }
      {
        key = "right";
        command = "next_column";
      }
      {
        key = "l";
        command = "next_column";
      }
      {
        key = "right";
        command = "enter_directory";
      }
      {
        key = "l";
        command = "enter_directory";
      }
      {
        key = "n";
        command = "next_found_item";
      }
      {
        key = "N";
        command = "previous_found_item";
      }
      {
        key = "g";
        command = "move_home";
      }
      {
        key = "G";
        command = "move_end";
      }
    ];
    settings = {
      allow_for_physical_item_deletion = "yes";
    };
  };
}
