{pkgs, ...}: {
  programs.rofi = {
    enable = true;
    package = pkgs.rofi-wayland;
    plugins = with pkgs; [
      rofi-emoji-wayland
    ];
    # pass = {
    #   enable = true;
    #   package = pkgs.rofi-pass-wayland;
    # };
    extraConfig = {
      modi = "run,drun,emoji,ssh";
      kb-primary-paste = "Control+V,Shift+Insert";
      kb-secondary-paste = "Control+v,Insert";
      show-icons = false;
      matching = "fuzzy";
      display-drun = "   Apps ";
      display-run = " Run ";
      run-display-format = "{name}";
      drun-display-format = "{icon} {name}";
    };
    theme = "dmenu-drop";
  };

  home.file.".config/rofi/dmenu.rasi" = {
    force = true;
    source = ./dmenu.rasi;
  };

  home.file.".config/rofi/dmenu-drop.rasi" = {
    force = true;
    source = ./dmenu-drop.rasi;
  };

  home.file.".config/rofi/dmenu-alert.rasi" = {
    force = true;
    source = ./dmenu-alert.rasi;
  };

  home.file.".config/rofi/askpass.rasi" = {
    force = true;
    source = ./askpass.rasi;
  };
}
