{
  pkgs,
  inputs,
  lib,
  ...
}: {
  programs.tmux = {
    enable = true;
    baseIndex = 1;
    keyMode = "vi";
    mouse = true;
    historyLimit = 5000;
    prefix = "C-a";
    plugins = with pkgs.tmuxPlugins; [
      {
        plugin = catppuccin;
        extraConfig =
          /*
          tmux
          */
          ''
            # set -g @catppuccin_flavor 'mocha' # latte, frappe, macchiato or mocha
            # set -g @catppuccin_window_left_separator "█"
            # set -g @catppuccin_window_middle_separator "█"
            # set -g @catppuccin_window_right_separator "█"
            # set -g @catppuccin_status_left_separator ""
            # set -g @catppuccin_status_modules_right "directory user host session"

            set -g @catppuccin_window_left_separator "█"
            set -g @catppuccin_window_middle_separator "█"
            set -g @catppuccin_window_right_separator "█"

            set -g @catppuccin_window_number_position "left"

            set -g @catppuccin_window_default_fill "number"
            set -g @catppuccin_window_default_text "#W"
            set -g @catppuccin_window_current_fill "number"
            set -g @catppuccin_window_current_text "#W"

            set -g @catppuccin_status_modules_right "directory session"

            set -g @catppuccin_status_left_separator "█"
            set -g @catppuccin_status_middle_separator "█"
            set -g @catppuccin_status_right_separator "█"
            set -g @catppuccin_status_connect_separator "no" # yes, no
            set -g @catppuccin_status_fill "icon"
          '';
      }
      {
        # plugin = inputs.tmux-sessionx.packages.${pkgs.system}.default;
        plugin = session-wizard;
        extraConfig =
          /*
          tmux
          */
          ''
            set -g @session-wizard 'o'
          '';
      }
    ];
    extraConfig =
      /*
      tmux
      */
      ''
        # fix terminal colors
        set -g default-terminal "screen-256color"
        set -ga terminal-overrides ',xterm-256color:Tc'

        set -g renumber-windows on # reorder numbers after closing a window

        set -g status-position top

        set -s escape-time 0 # make escape key faster (mostly for vi)

        bind-key -T copy-mode-vi 'v' send -X begin-selection     # Begin selection in copy mode.
        bind-key -T copy-mode-vi 'C-v' send -X rectangle-toggle  # Begin selection in copy mode.
        bind-key -T copy-mode-vi 'y' send -X copy-selection      # Yank selection in copy mode.
      '';
  };
}
