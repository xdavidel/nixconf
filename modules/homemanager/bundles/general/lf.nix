{
  inputs,
  pkgs,
  ...
}: {
  programs.lf = {
    enable = true;

    commands = {
      dragon-out = ''%${pkgs.xdragon}/bin/xdragon -a -x "$fx"'';
      destroy = ''%rm -rf "$f"'';
      editor-open = ''$$EDITOR $f'';
      chmod = ''
        ''${{
          echo -n "Mode Bits: "
          read ans

          for file in "$fx"; do
          chmod $ans $file
          done
        }}'';
      chown = ''
        ''${{
          echo -n "Owner: "
          read ans

          for file in "$fx"; do
          chown $ans:$ans $file
          done
        }}'';
    };

    keybindings = {
      "." = "set hidden!";
      "<enter>" = "open";
      B = ''''$${pkgs.binwalk}/bin/binwalk -e "$f"'';
      c = "";
      cw = ''''$${inputs.self.packages.${pkgs.system}.bulkmv}/bin/bulkmv "$fs"'';
      cm = "chmod";
      co = "chown";
      d = "";
      dd = "cut";
      dD = "delete";
      do = "dragon-out";
      D = "destroy";
      e = "editor-open";
      E = ''''$${pkgs.atool}/bin/aunpack "$f"'';
      m = "";
      md = "push %mkdir<space>";
      mf = "push %touch<space>";
      S = ''''$${inputs.self.packages.${pkgs.system}.hashes}/bin/hashes "$fx" | $EDITOR -'';
      V = ''''$${pkgs.bat}/bin/bat --paging=always --theme=gruvbox "$f"'';
      w = ''''$${inputs.self.packages.${pkgs.system}.setbg}/bin/setbg "$f"'';
    };

    settings = {
      preview = true;
      hidden = false;
      ignorecase = true;
      previewer = "${pkgs.ctpv}/bin/ctpv";
      cleaner = "${pkgs.ctpv}/bin/ctpvclear";
    };
  };
}
