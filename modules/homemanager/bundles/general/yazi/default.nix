{
  inputs,
  pkgs,
  config,
  ...
}: {
  programs.yazi = {
    enable = true;
    enableZshIntegration = true;
    enableBashIntegration = true;
    settings.plugin = {
      prepend_previewers = [
        {
          mime = "application/octet-stream";
          run = "binary";
        }
      ];
    };
    keymap = {
      input.prepend_keymap = [
      ];
      manager.prepend_keymap = [
        {
          on = ["b"];
          run = ''shell '${inputs.self.packages.${pkgs.system}.setbg}/bin/setbg "$1"' --confirm'';
          desc = "Calculate Hashes";
        }
        {
          on = ["c" "h"];
          run = ''shell '${inputs.self.packages.${pkgs.system}.hashes}/bin/hashes "$1" | $EDITOR -' --block --confirm'';
          desc = "Calculate Hashes";
        }
        {
          on = ["d" "d"];
          run = ''yank --cut'';
          desc = "Cut";
        }
        {
          on = ["d" "D"];
          run = ''remove --permanently'';
          desc = "Delete";
        }
        {
          on = ["D"];
          run = ''remove --permanently --force'';
          desc = "Force Delete";
        }
        {
          on = ["B"];
          run = ''shell '${pkgs.binwalk}/bin/binwalk -e "$1"' --confirm'';
          desc = "Binwalk";
        }
        {
          on = ["E"];
          run = ''shell '${pkgs.atool}/bin/aunpack "$1"' --confirm'';
          desc = "Extract";
        }
        {
          on = ["V"];
          run = ''shell '${pkgs.bat}/bin/bat --paging=always --theme=gruvbox "$1"' --block --confirm'';
          desc = "Extract";
        }
        {
          on = ["<C-d>"];
          run = ''shell '${pkgs.xdragon}/bin/xdragon -a -x "$1"' --confirm'';
          desc = "Drag & Drop";
        }
        {
          on = ["<C-s>"];
          run = ''shell "$SHELL" --block --confirm'';
          desc = "Open shell here";
        }
        {
          on = ["<Enter>"];
          run = ''plugin --sync smart-enter'';
          desc = "Enter the child directory, or open the file";
        }
      ];
    };
  };

  home.file.".config/yazi/init.lua" = {
    text =
      /*
      lua
      */
      ''
        require("session"):setup {
          sync_yanked = true,
        }
      '';
    force = true;
  };

  home.file.".config/yazi/plugins/smart-enter.yazi/init.lua" = {
    text =
      /*
      lua
      */
      ''
        return {
          entry = function()
            local h = cx.active.current.hovered
            ya.manager_emit(h and h.cha.is_dir and "enter" or "open", { hovered = true })
          end,
        }
      '';
    force = true;
  };

  home.file.".config/yazi/plugins/binary.yazi/init.lua" = {
    text =
      /*
      lua
      */
      ''

        dump = function(o)
           if type(o) == 'table' then
              local s = '{ '
              for k,v in pairs(o) do
                 if type(k) ~= 'number' then k = '"'..k..'"' end
                 s = s .. '['..k..'] = ' .. dump(v) .. ','
              end
              return s .. '} '
           else
              return tostring(o)
           end
        end
        return {
          peek = function(self)
            local child = Command("${pkgs.unixtools.xxd}/bin/xxd")
              :args({
                "-a",
                "-g",
                "1",
                "-R",
                "always",
                tostring(self.file.url),
              })
              :stdout(Command.PIPED)
              :stderr(Command.PIPED)
              :spawn()
            if not child then
              return
            end

            local limit = self.area.h
            local i, lines = 0, ""
            repeat
              local next, event = child:read_line()
              if event == 1 then
                return self:fallback_to_builtin()
              elseif event ~= 0 then
                break
              end

              i = i + 1
              if i > self.skip then
                lines = lines .. next
              end
            until i >= self.skip + limit

            child:start_kill()
            if self.skip > 0 and i < self.skip + limit then
              ya.manager_emit("peek", { math.max(0, i - limit), only_if = self.file.url, upper_bound = true })
            else
              lines = lines:gsub("\t", string.rep(" ", PREVIEW.tab_size))
              ya.preview_widgets(self, { ui.Paragraph.parse(self.area, lines) })
            end
          end,
          seek = function(self)
            local h = cx.active.current.hovered
            if h and h.url == self.file.url then
              local step = math.floor(units * self.area.h / 10)
              ya.manager_emit("peek", {
                math.max(0, cx.active.preview.skip + step),
                only_if = self.file.url,
              })
            end
          end,
          fallback_to_builtin = function()
            local err, bound = ya.preview_code(self)
            if bound then
              ya.manager_emit("peek", { bound, only_if = self.file.url, upper_bound = true })
            elseif err and not err:find("cancelled", 1, true) then
              ya.preview_widgets(self, {
                ui.Paragraph(self.area, { ui.Line(err):reverse() }),
              })
            end
          end
        }
      '';
    force = true;
  };

  home.packages = with pkgs;
    [
      exiftool # not required but helpful
    ]
    ++ (
      if (config.mHomeManager.alacritty.enable == true)
      then [
        ueberzugpp
      ]
      else []
    );
}
