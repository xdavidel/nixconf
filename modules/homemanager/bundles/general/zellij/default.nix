{pkgs, ...}: {
  programs.zellij = {
    enable = true;
    package = pkgs.unstable.zellij;
    # enableBashIntegration = true;
    # enableZshIntegration = true;
  };

  # not all settings can be configured using nix
  home.file.".config/zellij/config.kdl" = {
    force = true;
    source = ./config.kdl;
  };
}
