{
  inputs,
  pkgs,
  config,
  ...
}: let
  myAliases =
    {
      adbshell = "${pkgs.android-tools}/bin/adb shell -t 'export HOME=/data/local/tmp'";
      cat = "${pkgs.bat}/bin/bat -pp";
      diff = "${pkgs.diffutils}/bin/diff --color=auto";
      ftrace = "${pkgs.strace}/bin/strace -e trace=open,openat,connect,accept";
      grep = "${pkgs.gnugrep}/bin/grep --color=auto";
      ip = "${pkgs.iproute2}/bin/ip -color=auto";
      l = "${pkgs.eza}/bin/eza -l --group-directories-first";
      ll = "${pkgs.eza}/bin/eza -la --group-directories-first";
      la = "${pkgs.eza}/bin/eza -a --group-directories-first";
      ls = "${pkgs.eza}/bin/eza --group-directories-first";
      lg = "${pkgs.lazygit}/bin/lazygit";
      man = "batman";
      q = "exit";
      yt = "${pkgs.yt-dlp}/bin/yt-dlp --embed-metadata -i";
      ytv = "${pkgs.yt-dlp}/bin/yt-dlp -o '%(upload_date)s-%(title)s.%(ext)s'";
      yta = "${pkgs.yt-dlp}/bin/yt-dlp -x -f bestaudio/best --restrict-filenames --audio-format mp3";
      ".." = "cd ..";
    }
    // (
      if config.programs.zoxide.enable == true
      then {
        zz = "__zoxide_zi";
      }
      else {}
    );
  customShellRc =
    /*
    bash
    */
    ''
      [ -f "$HOME/.config/shell_custom_rc" ] && . "$HOME/.config/shell_custom_rc"
    '';
  lf =
    /*
    sh
    */
    ''
      lf() {
        tmp="$(mktemp -t "yazi-cwd.XXXXXX")"
        ${config.programs.yazi.package}/bin/yazi "$@" --cwd-file="$tmp"
        if cwd="$(< "$tmp")" && [ "$cwd" != "$PWD" ]; then
          cd "$cwd"
        fi
        rm -f "$tmp"
      }
    '';
  cdi =
    /*
    sh
    */
    ''
      cdi () {
        cd "$(dirname "$(${pkgs.fzf}/bin/fzf)")"
      }
    '';
  pjump =
    /*
    sh
    */
    ''
      cdp () {
        cd "$(${inputs.self.packages.${pkgs.system}.pjump}/bin/pjump list)"
      }
      save () {
        ${inputs.self.packages.${pkgs.system}.pjump}/bin/pjump save
      }
    '';
  lsf =
    /*
    sh
    */
    ''
      lsf () {
        ${inputs.self.packages.${pkgs.system}.listfiles}/bin/listfiles
      }
    '';
in {
  programs.readline = {
    enable = true;
    extraConfig =
      /*
      sh
      */
      ''
        set editing-mode vi
        $if mode=vi
        set show-mode-in-prompt on

        $if term=linux
        set vi-cmd-mode-string "[N] \1\e[?0c\2"
        set vi-ins-mode-string "[I] \1\e[?8c\2"
        $else
        set vi-cmd-mode-string "\1\e[30;41m\2[N]\1\e[m\2 \1\e[2 q\2"
        set vi-ins-mode-string "\1\e[30;42m\2[I]\1\e[m\2 \1\e[6 q\2"
        $endif
        set keymap vi-command
          # these are for vi-command mode
          Control-l: clear-screen
          Control-a: beginning-of-line

          set keymap vi-insert
          # these are for vi-insert mode
          Control-l: clear-screen
          Control-a: beginning-of-line

          $endif

        # Fast vi mode switching
        set keyseq-timeout 1

        # Search history for partial commands completions
        "\e[A": history-search-backward
        "\e[B": history-search-forward

        # Single tab completion
        set show-all-if-ambiguous on
        set completion-ignore-case On
      '';
  };

  programs.bash = {
    enable = true;
    shellAliases = myAliases;
    enableCompletion = true;
    historyControl = ["erasedups" "ignoredups" "ignorespace"];
    bashrcExtra =
      /*
      bash
      */
      ''
        ${lf}
        ${cdi}
        ${pjump}
        ${lsf}
        bind '"\C-f":"cdi\C-m"'
        bind '"\C-p":"lsf\C-m"'
        bind '"\C-o":"lf\C-m"'
        bind '"\C-e":"edit-and-execute-command"'

        ${customShellRc}
      '';
  };

  programs.zsh = {
    enable = true;
    shellAliases = myAliases;
    autosuggestion.enable = true;
    enableCompletion = true;
    syntaxHighlighting.enable = true;
    dotDir = ".config/zsh";
    history = {
      ignoreAllDups = true;
      ignorePatterns = ["yta *" "ytv *"];
      ignoreSpace = true;
      path = "$ZDOTDIR/.zsh_history";
      share = false;
    };

    initExtra =
      /*
      bash
      */
      ''
        zstyle ':completion:*' menu select
        zstyle ':completion:*' matcher-list ''' 'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'
        zstyle ':completion:*' rehash true

        zstyle ':completion:*' accept-exact '*(N)'
        zstyle ':completion:*' use-cache on
        zstyle ':completion:*' cache-path ~/.cache/zsh/cache

        # fix vi mode cursor
        zle-keymap-select() {
            case "$KEYMAP" in
                vicmd)
                    echo -ne '\e[1 q' ;;
                viins|main)
                    echo -ne '\e[5 q' ;;
            esac
        }
        zle-line-init() {
            zle -K viins
        }

        zle -N zle-keymap-select
        zle -N zle-line-init

        ${lf}
        ${cdi}
        ${pjump}
        ${lsf}
        bindkey -s '^f' 'cdi\n'
        bindkey -s '^p' 'lsf\n'
        bindkey -s '^o' 'lf\n'

        # fix backspace delete after vi mode
        bindkey "^?" backward-delete-char

        # delete previous word with ctrl+backspace
        bindkey "^H" backward-kill-word

        # Ctrl + Right key
        bindkey '^[[1;5C'  forward-word

        # Ctrl + Left key
        bindkey '^[[1;5D'  backward-word

        bindkey '\e[5~'    history-beginning-search-backward        # Page up key
        bindkey '\e[6~'    history-beginning-search-forward         # Page down key


        bindkey '^[[7~'   beginning-of-line                         # Home key
        bindkey '^[[H'    beginning-of-line                         # Home key
        bindkey '^[[8~'   end-of-line                               # End key
        bindkey '^[[F'    end-of-line                               # End key

        # Edit line in vim with ctrl-e:
        autoload edit-command-line; zle -N edit-command-line
        bindkey '^e' edit-command-line

        # ci", ci', ci`, di", etc
        autoload -U select-quoted
        zle -N select-quoted
        for m in visual viopp; do
          for c in {a,i}{\',\",\`}; do
            bindkey -M $m $c select-quoted
          done
        done

        # ci{, ci(, ci<, di{, etc
        autoload -U select-bracketed
        zle -N select-bracketed
        for m in visual viopp; do
          for c in {a,i}''${(s..)^:-'()[]{}<>bB'}; do
            bindkey -M $m $c select-bracketed
          done
        done

        ${customShellRc}
      '';
  };

  programs.starship = {
    enable = true;
    enableBashIntegration = true;
    enableZshIntegration = true;
  };

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
    enableBashIntegration = true;
    enableZshIntegration = true;
  };

  programs.zoxide = {
    enable = true;
    # options = ["--cmd cd"];
    enableBashIntegration = true;
    enableZshIntegration = true;
  };
}
