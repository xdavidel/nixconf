{
  config,
  inputs,
  lib,
  outputs,
  pkgs,
  ...
}: {
  imports = [
    ./shells.nix
    ./tmux
    ./yazi
    ./zellij
  ];

  colorScheme = inputs.nix-colors.colorSchemes.catppuccin-mocha;

  programs.bat = {
    enable = true;
    extraPackages = with pkgs.bat-extras; [batman];
  };

  programs.pandoc = {
    enable = true;
  };

  programs.ssh = {
    enable = true;
    compression = true;
    forwardAgent = true;
    hashKnownHosts = true;
    addKeysToAgent = "yes";
    serverAliveInterval = 15;
    includes = ["~/.ssh/config.d/*.conf"];
  };

  programs.fzf = {
    enable = true;
    enableBashIntegration = true;
    enableZshIntegration = true;
  };

  programs.git = {
    enable = true;
    aliases = {
      "d" = "diff";
      "gl" = "config --global -l";
      "co" = "checkout";
      "cm" = "commit -m";
      "mr" = "merge";
      "st" = "status -sb";
      "ll" = "log --oneline";
      "rv" = "remote -v";
      "se" = "!git rev-list --all | ${pkgs.findutils}/bin/xargs git grep -F";
      "last" = "log -1 HEAD --stat";
      "site" =
        /*
        sh
        */
        ''          !f() {
                      CURRENT_BRANCH=$(git branch --show-current)
                      REPO_URL="$(git config remote.origin.url)"
                      case "$REPO_URL" in
                        git@*) REPO_URL="''${REPO_URL%%.git}"
                              REPO_URL="''${REPO_URL//://}"
                              REPO_URL="''${REPO_URL//git@/https://}" ;;
                      esac

                      git rev-parse --quiet --verify "refs/remotes/origin/''${CURRENT_BRANCH}" >/dev/null && \
                        REPO_URL="''${REPO_URL}/tree/''${CURRENT_BRANCH}"

                      if [ -n "$SSH_CONNECTION" ]; then
                        printf "%s\n%s\n" "You are on a remote device. The project URL is:" "$REPO_URL"
                      else
                        xdg-open "$REPO_URL" >/dev/null 2>&1 || echo "Failed to open browser. The project URL is: $REPO_URL"
                      fi
                    }
                    f
        '';
    };
    package = pkgs.gitFull;
    extraConfig = {
      credential.helper = "${pkgs.gitFull}/bin/git-credential-libsecret";
      push.autoSetupRemote = true;
    };
  };

  programs.home-manager.enable = true;
  manual.manpages.enable = true;

  home.packages = with pkgs; [
    chafa
    clang
    ffmpeg
    file
    gdb
    glow
    htop
    imagemagick
    inputs.self.packages.${pkgs.system}.chs
    inputs.self.packages.${pkgs.system}.flatdir
    inputs.self.packages.${pkgs.system}.getwal
    inputs.self.packages.${pkgs.system}.vcompress
    jq
    lazygit
    newsboat
    ripgrep
    unixtools.xxd
    yt-dlp
  ];

  xdg = {
    configFile."mimeapps.list".force = true;
    userDirs = {
      enable = true;
      createDirectories = true;
    };
    mimeApps.enable = true;
  };
}
