{
  lib,
  config,
  pkgs,
  inputs,
  ...
}: {
  home.file.".config/awesome/theme.lua" = {
    force = true;
    text =
      /*
      lua
      */
      ''
        local theme_assets = require("beautiful.theme_assets")

        local xresources = require("beautiful.xresources")
        local dpi = xresources.apply_dpi

        local gfs = require("gears.filesystem")
        local themes_path = gfs.get_themes_dir()

        local homedir = os.getenv("HOME")

        local system_colors =  {
          foreground   =  '#cdd6f4',
          background   =  '#272929',
          cursor       =  '#c5c8c6',

          black_d      =  '#181825',
          black_l      =  '#1e1e2e',

          red_d        =  '#a54242',
          red_l        =  '#f38ba8',

          green_d      =  '#8c9440',
          green_l      =  '#a6e3a1',

          yellow_d     =  '#fab387',
          yellow_l     =  '#f9e2af',

          blue_d       =  '#89b4fa',
          blue_l       =  '#b4befe',

          magenta_d    =  '#75507b',
          magenta_l    =  '#cba6f7',

          cyan_d       =  '#5e8d87',
          cyan_l       =  '#94e2d5',

          white_d      =  '#f5e0dc',
          white_l      =  '#c5c8c6',
        }

        local theme = {
          font            = "mono 10",
          taglist_font    = "mono 10",
          fg_normal       = system_colors.foreground,
          fg_focus        = system_colors.black_d,
          fg_urgent       = system_colors.foreground,
          fg_minimize     = system_colors.foreground,
          bg_normal       = system_colors.background,
          bg_minimize     = system_colors.black_l,
          bg_focus        = system_colors.magenta_d,
          bg_urgent       = system_colors.red_d,
          bg_systray      = system_colors.background,
          border_normal   = system_colors.black_l,
          border_focus    = system_colors.red_l,
          border_width    = dpi(3),
          useless_gap     = dpi(4),
          gap_single_client = false;
        }

        -- Generate taglist squares:
        local taglist_square_size = dpi(4)
        theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
          taglist_square_size, theme.fg_normal
        )
        theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
          taglist_square_size, theme.fg_normal
        )

        -- Variables set for theming the menu:
        theme.menu_submenu_icon = themes_path.."default/submenu.png"
        theme.menu_height = dpi(25)
        theme.menu_width  = dpi(200)

        -- Define the image to load
        theme.titlebar_close_button_normal = themes_path.."default/titlebar/close_normal.png"
        theme.titlebar_close_button_focus  = themes_path.."default/titlebar/close_focus.png"

        theme.titlebar_minimize_button_normal = themes_path.."default/titlebar/minimize_normal.png"
        theme.titlebar_minimize_button_focus  = themes_path.."default/titlebar/minimize_focus.png"

        theme.titlebar_ontop_button_normal_inactive = themes_path.."default/titlebar/ontop_normal_inactive.png"
        theme.titlebar_ontop_button_focus_inactive  = themes_path.."default/titlebar/ontop_focus_inactive.png"
        theme.titlebar_ontop_button_normal_active = themes_path.."default/titlebar/ontop_normal_active.png"
        theme.titlebar_ontop_button_focus_active  = themes_path.."default/titlebar/ontop_focus_active.png"

        theme.titlebar_sticky_button_normal_inactive = themes_path.."default/titlebar/sticky_normal_inactive.png"
        theme.titlebar_sticky_button_focus_inactive  = themes_path.."default/titlebar/sticky_focus_inactive.png"
        theme.titlebar_sticky_button_normal_active = themes_path.."default/titlebar/sticky_normal_active.png"
        theme.titlebar_sticky_button_focus_active  = themes_path.."default/titlebar/sticky_focus_active.png"

        theme.titlebar_floating_button_normal_inactive = themes_path.."default/titlebar/floating_normal_inactive.png"
        theme.titlebar_floating_button_focus_inactive  = themes_path.."default/titlebar/floating_focus_inactive.png"
        theme.titlebar_floating_button_normal_active = themes_path.."default/titlebar/floating_normal_active.png"
        theme.titlebar_floating_button_focus_active  = themes_path.."default/titlebar/floating_focus_active.png"

        theme.titlebar_maximized_button_normal_inactive = themes_path.."default/titlebar/maximized_normal_inactive.png"
        theme.titlebar_maximized_button_focus_inactive  = themes_path.."default/titlebar/maximized_focus_inactive.png"
        theme.titlebar_maximized_button_normal_active = themes_path.."default/titlebar/maximized_normal_active.png"
        theme.titlebar_maximized_button_focus_active  = themes_path.."default/titlebar/maximized_focus_active.png"

        -- You can use your own layout icons like this:
        theme.layout_fairh = themes_path.."default/layouts/fairhw.png"
        theme.layout_fairv = themes_path.."default/layouts/fairvw.png"
        theme.layout_floating  = themes_path.."default/layouts/floatingw.png"
        theme.layout_magnifier = themes_path.."default/layouts/magnifierw.png"
        theme.layout_max = themes_path.."default/layouts/maxw.png"
        theme.layout_fullscreen = themes_path.."default/layouts/fullscreenw.png"
        theme.layout_tilebottom = themes_path.."default/layouts/tilebottomw.png"
        theme.layout_tileleft   = themes_path.."default/layouts/tileleftw.png"
        theme.layout_tile = themes_path.."default/layouts/tilew.png"
        theme.layout_tiletop = themes_path.."default/layouts/tiletopw.png"
        theme.layout_spiral  = themes_path.."default/layouts/spiralw.png"
        theme.layout_dwindle = themes_path.."default/layouts/dwindlew.png"
        theme.layout_cornernw = themes_path.."default/layouts/cornernww.png"
        theme.layout_cornerne = themes_path.."default/layouts/cornernew.png"
        theme.layout_cornersw = themes_path.."default/layouts/cornersww.png"
        theme.layout_cornerse = themes_path.."default/layouts/cornersew.png"

        -- Generate Awesome icon:
        theme.awesome_icon = theme_assets.awesome_icon(
          theme.menu_height, theme.bg_focus, theme.fg_focus
        )

        -- Define the icon theme for application icons. If not set then the icons
        -- from /usr/share/icons and /usr/share/icons/hicolor will be used.
        theme.icon_theme = nil

        return theme
      '';
  };
}
