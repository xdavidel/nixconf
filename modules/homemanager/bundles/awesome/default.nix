{
  lib,
  config,
  pkgs,
  inputs,
  ...
}: {
  imports = [
    ./rc.nix
    ./theme.nix
  ];

  xsession.windowManager.awesome.enable = true;

  programs.autorandr = {
    enable = true;

    hooks = {
      postswitch = {
        "notify-profile" = ''
          ${pkgs.libnotify}/bin/notify-send -i display "Display profile" "$AUTORANDR_CURRENT_PROFILE"
        '';
        "keyboard" = ''${pkgs.xorg.xset}/bin/xset r rate 300 50'';
        "change-background" = ''${inputs.self.packages.${pkgs.system}.x11}/bin/xsetbg'';
      };
    };
  };

  home.packages = with pkgs; [
    inputs.self.packages.${pkgs.system}.sysstat
    inputs.self.packages.${pkgs.system}.x11
  ];
}
