{
  lib,
  config,
  pkgs,
  inputs,
  ...
}: {
  home.file.".config/awesome/rc.lua" = let
    dmc = "${inputs.self.packages.${pkgs.system}.dmc}/bin/dmc";
    nmctl = "${inputs.self.packages.${pkgs.system}.nmctl}/bin/nmctl";
    maimpick = "${inputs.self.packages.${pkgs.system}.x11}/bin/maimpick";
    locker = "${pkgs.i3lock-fancy-rapid}/bin/i3lock-fancy-rapid 5 3";
    brightnessctl = "${pkgs.brightnessctl}/bin/brightnessctl";
  in {
    force = true;
    text =
      /*
      lua
      */
      ''
          -- Standard awesome library
          local gears = require("gears")
          local awful = require("awful")
          require("awful.autofocus")

          -- Widget and layout library
          local wibox = require("wibox")

          -- Theme handling library
          local beautiful = require("beautiful")

          -- Notification library
          local naughty = require("naughty")
          local menubar = require("menubar")
          local hotkeys_popup = require("awful.hotkeys_popup")
          -- Enable hotkeys help widget for VIM and other apps
          -- when client with a matching name is opened:
          require("awful.hotkeys_popup.keys")

          -- {{{ Error handling
          -- Check if awesome encountered an error during startup and fell back to
          -- another config (This code will only ever execute for the fallback config)
          if awesome.startup_errors then
            naughty.notify({ preset = naughty.config.presets.critical,
            title = "Oops, there were errors during startup!",
            text = awesome.startup_errors })
          end

          -- Handle runtime errors after startup
          do
            local in_error = false
            awesome.connect_signal("debug::error", function (err)
              -- Make sure we don't go into an endless error loop
              if in_error then return end
              in_error = true

              naughty.notify({ preset = naughty.config.presets.critical,
                title = "Oops, an error happened!",
                text = tostring(err) })
                in_error = false
            end)
          end
          -- }}}

          -- {{{ Helper functions

          -- On the fly useless gaps change (from lain)
          local function useless_gaps_resize(thatmuch, s, t)
            local scr = s or awful.screen.focused()
            local tag = t or scr.selected_tag
            local delta = tonumber(thatmuch)
            if delta == 0 then
              -- reset to default
              tag.gap = beautiful.useless_gap
            else
              tag.gap = tag.gap + tonumber(thatmuch)
            end
              awful.layout.arrange(scr)
          end


          local switch_screen = function(direction)
            awful.screen.focus_relative(direction)
            naughty.notify({
                title = "Switch Screen",
            })
          end

          -- Toggle titlebar on or off depending on s. Creates titlebar if it doesn't exist
          local function setTitlebar(client, s)
            if s then
              if client.titlebar == nil then
                client:emit_signal("request::titlebars", "rules", {})
              end
              awful.titlebar.show(client)
            else
              awful.titlebar.hide(client)
            end
          end

          local alpha = 'CC'
          local altbackground = '#4d4d4d' .. alpha

          local TAG_SP = "SP"
          local function tagviewswitch(direction)
            local s = awful.screen.focused()
            local tags = s.tags

            local next_index = s.selected_tag.index + direction

            for i = next_index, direction > 0 and #tags or 1, direction do
              local t = tags[i]
              if t.name ~= TAG_SP and #t:clients() > 0 then
                t:view_only()
                return
              end
            end

            -- if reached here wrap around
            if direction > 0 then
              next_index = 1
            else
              next_index = #tags
            end

            for i = next_index, direction > 0 and #tags or 1, direction do
              local t = tags[i]
              if t.name ~= TAG_SP and #t:clients() > 0 then
                t:view_only()
                return
              end
            end
          end

          local function tagview(t)
            if t and t.name ~= TAG_SP then
              t:view_only()
            end
          end

          local function tagviewtoggle(t)
            if t and t.name ~= TAG_SP then
              awful.tag.viewtoggle(t)
            end
          end

          local function set_wallpaper(s)
            -- Wallpaper
            if beautiful.wallpaper then
              local wallpaper = beautiful.wallpaper
              -- If wallpaper is a function, call it with the screen
              if type(wallpaper) == "function" then
                wallpaper = wallpaper(s)
              end
              gears.wallpaper.maximized(wallpaper, s, true)
            end
          end

          local padding = 3

          local create_scriptwidget = function(cmd, watch_interval, signal)
              local smart_widget

              local script_widget

              if watch_interval > 0 then
                script_widget = awful.widget.watch(
                  cmd,
                  watch_interval,
                  function(widget, stdout)
                    smart_widget.visible = string.len(stdout) > 1 and true or false
                    widget:set_markup(stdout)
                  end
                )
              else
                script_widget = wibox.widget.textbox()
                awful.spawn.easy_async_with_shell(cmd, function(out)
                  smart_widget.visible = string.len(out) > 1 and true or false
                  script_widget:set_text(out)
                end)
              end

              smart_widget = wibox.container.margin(
                wibox.container.background(
                  wibox.container.margin(
                    script_widget,
                    padding,
                    padding,
                    0,
                    0
                  ),
                  altbackground
                ),
                0,
                padding * 2,
                0,
                0
              )

              smart_widget:buttons(
                gears.table.join(
                  smart_widget:buttons(),
                  awful.button(
                    {}, 1, nil,
                    function () awful.spawn(cmd .. " 1") end
                  ),
                  awful.button(
                    {}, 2, nil,
                    function () awful.spawn(cmd .. " 2") end
                  ),
                  awful.button(
                    {}, 3, nil,
                    function () awful.spawn(cmd .. " 3") end
                  ),
                  awful.button(
                    {}, 4, nil,
                    function () awful.spawn(cmd .. " 4") end
                  ),
                  awful.button(
                    {}, 5, nil,
                    function () awful.spawn(cmd .. " 5") end
                  ),
                  awful.button(
                    { "Shift" }, 1, nil,
                    function () awful.spawn(cmd .. " 6") end
                  )
                )
              )

              if signal then
                awesome.connect_signal(signal, function()
                  awful.spawn.easy_async_with_shell(cmd, function(out)
                    smart_widget.visible = string.len(out) > 1 and true or false
                    script_widget:set_markup(out)
                  end)
                end)
              end

           return smart_widget
          end

          local create_smart_widget = function(status, args)
            local args = args or {}
            local widget
            local update_text = function()
              awful.spawn.easy_async_with_shell(status, function(stdout)
                widget.visible = string.len(stdout) > 1 and true or false
                local text = args.format and args.format(stdout) or stdout
                local icon = args.icon and args.icon(text) or ""
                widget:set_markup(string.format(" %s %s ", icon, text))
              end)
            end

            if args.interval then
              widget = awful.widget.watch(
                status,
                args.interval,
                function(w, stdout)
                  w.visible = string.len(stdout) > 1 and true or false
                  local text = args.format and args.format(stdout) or stdout
                  local icon = args.icon and args.icon(text) or ""
                  w:set_markup(string.format(" %s %s ", icon, text))
                end)
            else
              widget = wibox.widget.textbox()
            end

            update_text()

            widget_wrapper = wibox.container.margin(
              wibox.container.background(
                wibox.container.margin(
                  widget,
                  padding,
                  padding,
                  0,
                  0
                ),
                altbackground
              ),
              0,
              padding * 2,
              0,
              0
            )

            widget_wrapper:buttons(
              gears.table.join(
                widget_wrapper:buttons(),
                awful.button(
                  {}, 1, nil,
                  function ()
                    if not args.mouse_left then return end
                    awful.spawn(args.mouse_left)
                    update_text()
                  end
                ),
                awful.button(
                  {}, 2, nil,
                  function ()
                    if not args.mouse_middle then return end
                    awful.spawn(args.mouse_middle)
                    update_text()
                  end
                ),
                awful.button(
                  {}, 3, nil,
                  function ()
                    if not args.mouse_right then return end
                    awful.spawn(args.mouse_right)
                    update_text()
                  end
                ),
                awful.button(
                  {}, 4, nil,
                  function ()
                    if not args.mouse_wheel_up then return end
                    awful.spawn(args.mouse_wheel_up)
                    update_text()
                  end
                ),
                awful.button(
                  {}, 5, nil,
                  function ()
                    if not args.mouse_wheel_down then return end
                    awful.spawn(args.mouse_wheel_down)
                    update_text()
                  end
                )
              )
            )

            if args.signal then
              awesome.connect_signal(args.signal, function()
                update_text()
              end)
            end

            return widget_wrapper
          end

          function restore_windows_to_desired_screen(new_screen)
            for _, c in ipairs(client.get()) do
              if not (c.desired_screen == nil) then
                tag_name = c.first_tag.name
                c:move_to_screen(c.desired_screen)
                tag = awful.tag.find_by_name(c.screen, tag_name)
                if not (tag == nil) then
                  c:move_to_tag(tag)
                end
                -- now clear the "desired_screen"
                c.desired_screen = nil
              end
            end
          end

          -- }}}

          -- {{{ startup
          awful.spawn.easy_async("${pkgs.autorandr}/bin/autorandr -c -d default", function(stdout, stderr, reason, exit_code)
            awful.spawn("${pkgs.xorg.xset}/bin/xset r rate 300 50")
            awful.spawn.single_instance("${pkgs.xcompmgr}/bin/xcompmgr")
            awful.spawn.easy_async("${pkgs.procps}/bin/pkill xidlehook", function(stdout, stderr, reason, exit_code)
              awful.spawn("${pkgs.xidlehook}/bin/xidlehook --not-when-fullscreen --not-when-audio --timer 360 '${locker}' ' ' --timer 360 'systemctl suspend' ' '")
            end)

            local update_music_bar = function(line)
              awesome.emit_signal("music::update")
            end

            awful.spawn.easy_async("${pkgs.procps}/bin/pkill -f 'mpc idleloop'", function(stdout, stderr, reason, exit_code)
              awful.spawn.with_line_callback("${pkgs.mpc-cli}/bin/mpc idleloop", {
                stdout = update_music_bar,
                stderr = update_music_bar,
              })
            end)
          end)
          -- }}}


          -- {{{ Variable definitions
          -- Themes define colours, icons, font and wallpapers.
          beautiful.init(gears.filesystem.get_configuration_dir() .. "theme.lua")
          -- beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")

          -- This is used later as the default terminal and editor to run.
          terminal = os.getenv("TERMINAL") or "xterm"
          editor = os.getenv("EDITOR") or "vi"
          editor_cmd = terminal .. " -e " .. editor

          -- Default modkey.
          -- Usually, Mod4 is the key with a logo between Control and Alt.
          -- If you do not like this or do not have such a key,
          -- I suggest you to remap Mod4 to another key using xmodmap or other tools.
          -- However, you can use another modifier like Mod1, but it may interact with others.
          modkey = "Mod4"
          altkey = "Mod1"
          sftkey = "Shift"
          ctlkey = "Control"

          scratchpad_name = "scratchpad"

          -- Table of layouts to cover with awful.layout.inc, order matters.
          awful.layout.layouts = {
            awful.layout.suit.tile,
            awful.layout.suit.tile.bottom,
            awful.layout.suit.spiral,
            awful.layout.suit.fair.horizontal,
            awful.layout.suit.spiral.dwindle,
            awful.layout.suit.max,
            awful.layout.suit.magnifier,
            awful.layout.suit.floating,
                -- awful.layout.suit.tile.left,
                -- awful.layout.suit.tile.top,
                -- awful.layout.suit.fair,
                -- awful.layout.suit.max.fullscreen,
                -- awful.layout.suit.corner.nw,
                -- awful.layout.suit.corner.ne,
                -- awful.layout.suit.corner.sw,
                -- awful.layout.suit.corner.se,
              }
          -- }}}

          -- {{{ Menu
          -- Create a launcher widget and a main menu
          myawesomemenu = {
            { "Titlebars",
              function()
                for _,c in ipairs(client.get()) do
                  awful.titlebar.toggle(c)
                end
              end
            },
            { "Hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
            { "Manual", terminal .. " -e man awesome" },
            { "Edit Config", editor_cmd .. " " .. awesome.conffile },
            { "Restart", awesome.restart },
            { "Quit", function() awesome.quit() end },
          }

          mymainmenu = awful.menu({
            items = {
              { "Awesome", myawesomemenu   },
              { "Virtual Keyboard", "'''${pkgs.xvkbd}/bin/xvkbd " },
              { "Terminal", terminal         },
              { "Exit", exitmenu             },
            }
          })

          mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
              menu = mymainmenu })

            -- Menubar configuration
            menubar.utils.terminal = terminal -- Set the terminal for applications that require it
            -- }}}

        -- Keyboard map indicator and switcher
          mykeyboardlayout = awful.widget.keyboardlayout()

          -- {{{ Wibar
          local desktoptext = wibox.widget.textbox("💻")
          desktoptext:buttons(
            gears.table.join(
              desktoptext:buttons(),
              awful.button(
                {}, 1, nil,
                function () mouse.screen.selected_tag.selected = false end
              )
            )
          )

          local showdesktop = wibox.container.background(
            wibox.container.margin(
              desktoptext,
              padding,
              padding,
              0,
              0
            ),
            altbackground
          )

          local music_widget = create_scriptwidget("music", 0, "music::update")

          local weather_widget = create_scriptwidget("weather", 18000)

          -- memory widget
          local mem_widget = create_scriptwidget("mem", 30)

          -- memory widget
          local cpu_widget = create_scriptwidget("cpu", 10)

          -- memory widget
          local bat_widget = create_scriptwidget("battery", 10)
          -- local bat_widget = create_smart_widget("cat /sys/class/power_supply/BAT?/capacity", {
          --   interval = 10,
          --   format = function(status)
          --     return status
          --   end,
          --   icon = function(status)
          --     value = tonumber(status)
          --     if value > 70 then
          --       return "<span color='#6db30c'> </span>"
          --     elseif value > 65 then
          --       return "<span color='#daa20f'> </span>"
          --     elseif value > 45 then
          --       return "<span color='#e16b13'> </span>"
          --     elseif value > 25 then
          --       return "<span color='#d83f21'> </span>"
          --     else
          --       return "<span color='#b30606'> </span>"
          --     end
          --   end,
          -- })

          -- volume widget
          local volume_widget = create_smart_widget("wpctl get-volume @DEFAULT_AUDIO_SINK@", {
            format = function(status)
              if status:find("[MUTED]") then
                return ""
              end
              tmp = status:gsub("Volume: ", "")
              return tostring(tonumber(tmp) * 100)
            end,
            icon = function(status)
              value = tonumber(status)
              if not value then
                return "🔇"
              end

              if value > 70 then
                return "🔊"
              elseif value > 30 then
                return "🔉"
              else
                return "🔈"
              end
            end,
            signal = "volume::update",
            mouse_left = "${pkgs.pavucontrol}/bin/pavucontrol",
            mouse_middle = "wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle",
            mouse_wheel_up = "wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 1%+",
            mouse_wheel_down = "wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 1%-",
          })

          local net_widget = create_scriptwidget("network", 20)

          local nettraf_widget = create_scriptwidget("nettraf", 2)
          local clock_widget = create_scriptwidget("clock", 60)
          local month_calendar = awful.widget.calendar_popup.month({start_sunday = true})
          month_calendar:attach( clock_widget, "br" )

          local mykeyboardlayout = wibox.container.margin(
            wibox.container.background(
              wibox.container.margin(
                wibox.widget {
                  wibox.widget.textbox(" 📖"),
                  awful.widget.keyboardlayout:new(),
                  layout  = wibox.layout.align.horizontal
                },
                padding,
                padding,
                0,
                0
              ),
              altbackground
            ),
            0,
            padding * 2,
            0,
            0
          )

          myseparator = wibox.widget.separator({
            orientation = "vertical",
            forced_width = 20,
          })

          -- Create a wibox for each screen and add it
          local taglist_buttons = gears.table.join(
            awful.button({ }, 1, function(t) tagview(t) end),
            awful.button({ modkey }, 1,
            function(t)
              if t.name ~= TAG_SP and client.focus then
                client.focus:move_to_tag(t)
              end
            end),

            awful.button({ }, 3, function(t) tagviewtoggle(t) end),
              awful.button({ modkey }, 3,
              function(t)
                if t.name ~= TAG_SP and client.focus then
                  client.focus:toggle_tag(t)
                end
              end),

              awful.button({ }, 4, function(_) tagviewswitch(1) end),
              awful.button({ }, 5, function(_) tagviewswitch(-1) end)
            )

            local currentclient
            local tags = (function()
              t = {}
              for i = 1, 19 do
                table.insert(t, string.format(" %d ", i))
              end
                return t
            end)()


            -- input is a client which we want to move
            local function get_tags_menu_items()
              local move_to_menu = {}
              -- iterate through all tags
              for _,tag in ipairs(tags) do
                -- create a menu item for each tag which consists of:
                --   * item title (first table element, we use tag's name here)
                --   * callback function which will be executed on item selection
                -- and then append this item to the output table
                table.insert(move_to_menu, {
                  " #" .. tag,
                  function ()
                    -- callback function is simple: just move client to the selected tag
                    currentclient:move_to_tag(awful.tag.find_by_name(awful.screen.focused(),tag))
                  end,
                })
              end

              local action_menu = {
                {"Close", function() currentclient:kill() end},
                {"Maximize", function() currentclient.maximized = true end},
                {"Minimize", function() currentclient.minimized = true end},
                {"Restore",
                function()
                  currentclient.minimized = false
                  currentclient.maximized = false
                  currentclient:raise()
                end },
              }

              local output = {}

              table.insert(output, {
                "Running Programs",
                awful.menu.client_list
              })

              table.insert(output, {
                "Move To Tag",
                move_to_menu
              })

              table.insert(output, {
                "Action",
                action_menu
              })

              table.insert(output, {
                "Swap Monitor",
                function() awful.client.movetoscreen(currentclient) end
              })

              return output
            end

            local tags_menu = awful.menu.new(get_tags_menu_items())

            local tasklist_buttons = gears.table.join(
              awful.button({}, 1,
              function(c)
                if c == client.focus then
                  c.minimized = true
                else
                  c:emit_signal("request::activate",
                    "tasklist",
                    { raise = true })
                end
              end),

              awful.button({}, 3,
                function(c)
                  currentclient = c
                  tags_menu:toggle()
                end),

              awful.button({}, 4,
                function()
                  awful.client.focus.byidx(1)
                end),

              awful.button({}, 5,
                function()
                  awful.client.focus.byidx(-1)
                end)
            )

            -- Create a wibox for each screen and add it
            local taglist_buttons = gears.table.join(
              awful.button({ }, 1, function(t) t:view_only() end),
              awful.button({ modkey }, 1, function(t)
                if client.focus then
                  client.focus:move_to_tag(t)
                end
              end),
              awful.button({ }, 3, awful.tag.viewtoggle),
              awful.button({ modkey }, 3, function(t)
                if client.focus then
                  client.focus:toggle_tag(t)
                end
              end)
              -- awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
              -- awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
            )

            -- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
            screen.connect_signal("property::geometry", set_wallpaper)
            awful.screen.connect_for_each_screen(function(s)
              -- Wallpaper
              set_wallpaper(s)

              -- Each screen has its own tag table.
              awful.tag(tags, s, awful.layout.layouts[1])

              restore_windows_to_desired_screen(s)

              -- Create a promptbox for each screen
              s.mypromptbox = awful.widget.prompt()

              -- Create an imagebox widget which will contain an icon indicating which layout we're using.
              -- We need one layoutbox per screen.
              s.mylayoutbox = awful.widget.layoutbox(s)
              s.mylayoutbox:buttons(gears.table.join(
                awful.button({ }, 1, function () awful.layout.inc( 1) end),
                awful.button({ }, 3, function () awful.layout.inc(-1) end),
                awful.button({ }, 4, function () awful.layout.inc( 1) end),
                awful.button({ }, 5, function () awful.layout.inc(-1) end))
              )

              -- Create a taglist widget
              s.mytaglist = awful.widget.taglist {
                screen  = s,
                -- filter  = awful.widget.taglist.filter.all,
                filter  = awful.widget.taglist.filter.noempty,
                buttons = taglist_buttons
              }

              -- Create a tasklist widget
              s.mytasklist = awful.widget.tasklist {
                screen  = s,
                filter  = awful.widget.tasklist.filter.currenttags,
                buttons = tasklist_buttons,
                layout  = {
                  spacing_widget = {
                    {
                      forced_width  = 5,
                      forced_height = 24,
                      thickness     = 0,
                      widget        = wibox.widget.separator
                    },
                    valign = 'center',
                    halign = 'center',
                    widget = wibox.container.place,
                  },
                  spacing = 1,
                  layout  = wibox.layout.fixed.horizontal
                },
                widget_template = {
                  {
                    {
                      id     = 'icon_role',
                      widget = wibox.widget.imagebox,
                    },
                    widget      = wibox.container.margin,
                    margins = 4,
                  },
                  id     = 'background_role',
                  widget = wibox.container.background,
                  create_callback = function(self, c, _, _) --luacheck: no unused args
                    -- default icon when none is found (ex. simple terminal)
                    if not c.icon then
                      self:get_children_by_id('icon_role')[1].image =
                        awful.util.get_configuration_dir() .. 'gear.svg'
                        end
                        awful.tooltip({
                            objects = { self },
                            timer_function = function() return c.name end,
                            })
                  end
                },
              }

              -- Create the wibox
              s.myTopWibox = awful.wibar({ position = "top", screen = s, bg = beautiful.bg_normal .. alpha})
              s.myBottomWibox = awful.wibar({ position = "bottom", screen = s, bg = beautiful.bg_normal .. alpha })

              -- Add widgets to the wibox
              s.myTopWibox:setup {
                layout = wibox.layout.align.horizontal,
                { -- Left widgets
                  layout = wibox.layout.fixed.horizontal,
                  s.mytaglist,
                  s.mypromptbox,
                },
                s.emptyspace,
                { -- Right widgets
                  layout = wibox.layout.fixed.horizontal,
                  music_widget,
                  weather_widget,
                  mem_widget,
                  cpu_widget,
                  volume_widget,
                  net_widget,
                  bat_widget,
                  s.mylayoutbox,
                },
              }

              s.myBottomWibox:setup {
                layout = wibox.layout.align.horizontal,
                { -- Left widgets
                  layout = wibox.layout.fixed.horizontal,
                  mylauncher,
                  myseparator,
                },
                s.mytasklist, -- Middle widget
                { -- Right widgets
                  layout = wibox.layout.fixed.horizontal,
                  mykeyboardlayout,
                  nettraf_widget,
                  clock_widget,
                  wibox.widget.systray(),
                  showdesktop
                },
              }

            end)
              -- }}}

        -- {{{ Mouse bindings
          root.buttons(gears.table.join(
                awful.button({ }, 3, function () mymainmenu:toggle() end)
                -- awful.button({ }, 4, awful.tag.viewnext),
                -- awful.button({ }, 5, awful.tag.viewprev)
                ))
            -- }}}

        -- {{{ Key bindings
          globalkeys = gears.table.join(
              awful.key({ modkey, ctlkey    }, "Left",   function() tagviewswitch(-1) end,
                {description = "view previous", group = "tag"}),
              awful.key({ modkey, ctlkey    }, "Right",  function() tagviewswitch(1) end,
                {description = "view next", group = "tag"}),
              awful.key({ modkey,           }, "Tab", awful.tag.history.restore,
                {description = "go back", group = "tag"}),

              awful.key({ modkey,           }, "BackSpace", function () mymainmenu:show() end,
                  {description = "show main menu", group = "awesome"}),

              -- focus by index
              awful.key({ modkey,           }, "Down",
                    function () awful.client.focus.byidx( 1) end,
                    {description = "focus next client", group = "client"}),
              awful.key({ modkey,           }, "Up",
                  function () awful.client.focus.byidx( -1) end,
                  {description = "focus prev client", group = "client"}),
              awful.key({ modkey,           }, "j",
                    function () awful.client.focus.byidx( 1) end,
                    {description = "focus next client", group = "client"}),
              awful.key({ modkey,           }, "k",
                  function () awful.client.focus.byidx( -1) end,
                  {description = "focus prev client", group = "client"}),

              -- swap by index
              awful.key({ modkey, sftkey    }, "Down",
                  function () awful.client.swap.byidx(  1) end,
                  {description = "swap with next lower client", group = "client"}),
              awful.key({ modkey, sftkey    }, "Up",
                  function () awful.client.swap.byidx( -1) end,
                  {description = "swap with next upper client", group = "client"}),
              awful.key({ modkey, sftkey    }, "j",
                  function () awful.client.swap.byidx(  1) end,
                  {description = "swap with next upper client", group = "client"}),
              awful.key({ modkey, sftkey    }, "k",
                  function () awful.client.swap.byidx( -1) end,
                  {description = "swap with next lower client", group = "client"}),

              -- focus screen
              awful.key({ modkey,           }, "Right",
                    function () switch_screen(-1) end,
                    {description = "focus the next screen", group = "screen"}),
              awful.key({ modkey,           }, "Left",
                  function () switch_screen(1) end,
                  {description = "focus the previous screen", group = "screen"}),

              -- Layout changing
              awful.key({ modkey                    }, "t",
                    function() awful.layout.set(awful.layout.suit.tile) end,
                    {description = "Set tiling layout", group = "layout"}),
              awful.key({ modkey, sftkey            }, "t",
                  function() awful.layout.set(awful.layout.suit.tile.bottom) end,
                  {description = "Set bstack layout", group = "layout"}),
              awful.key({ modkey                    }, "g",
                  function() awful.layout.set(awful.layout.suit.fair.horizontal) end,
                  {description = "Set grid layout", group = "layout"}),
              awful.key({ modkey, sftkey            }, "g",
                  function() awful.layout.set(awful.layout.suit.magnifier) end,
                  {description = "Set magnifier layout", group = "layout"}),
              awful.key({ modkey                    }, "s",
                  function() awful.layout.set(awful.layout.suit.spiral) end,
                  {description = "Set spiral layout", group = "layout"}),
              awful.key({ modkey, sftkey            }, "s",
                  function() awful.layout.set(awful.layout.suit.spiral.dwindle) end,
                  {description = "Set dwindle layout", group = "layout"}),
              awful.key({ modkey                    }, "u",
                  function() awful.layout.set(awful.layout.suit.max) end,
                  {description = "Set max layout", group = "layout"}),


              awful.key({ modkey, ctlkey }, "Up",
                  function()
                  local tag = awful.tag.selected()
                  for _, c in ipairs(tag:clients()) do
                  c.minimized = false
                  c:raise()
                  end
                  end,
                  { description = "Restore All", group = "client" }),

              awful.key({ modkey, ctlkey }, "Down",
                  function()
                  local tag = awful.tag.selected()
                  for _, c in ipairs(tag:clients()) do
                  c.minimized = true
                  end
                  end,
                  { description = "Minimize All", group = "client" }),

              -- Show / Hide wibox
              awful.key({ modkey,           }, "b",
                function ()
                  for s in screen do
                    s.myTopWibox.visible = not s.myTopWibox.visible
                    s.myBottomWibox.visible = not s.myBottomWibox.visible
                  end
                end,
                {description = "toggle wibox bars", group = "wibox"}),



              awful.key({ altkey,           }, "Tab",
                  function ()
                    local c = awful.client.focus.history.list[2]
                    if not c then
                      return
                    end

                    client.focus = c
                    local t = client.focus and client.focus.first_tag or nil
                    if t then
                      t:view_only()
                    end
                    c:raise()
                  end,
                {description = "go back", group = "client"}),

              -- Standard program
              awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
                {description = "open a terminal", group = "launcher"}),
              awful.key({ modkey, altkey }, "Return",
                  function()
                    local found = false
                    for _, c in pairs(client.get()) do
                      if c.instance == scratchpad_name then
                        found = true
                        c.hidden = not c.hidden
                        client.focus = c
                        c:raise()
                        break
                      end
                    end
                    if not found then
                      awful.spawn(terminal .. " --class " .. scratchpad_name)
                    end
                  end,
                { description = "Toggle scratchpad", group = "awesome" }),
              awful.key({ modkey, sftkey }, "Escape", awesome.restart,
                {description = "reload awesome", group = "awesome"}),
              awful.key({ modkey, "Shift"   }, "e", awesome.quit,
                {description = "quit awesome", group = "awesome"}),

              -- gaps
              awful.key({ modkey,           }, "o", function() useless_gaps_resize(4) end,
                {description = "focus next down", group = "client"}),
              awful.key({ modkey, sftkey    }, "o", function() useless_gaps_resize(-4) end,
                {description = "focus next up", group = "client"}),
              awful.key({ modkey, ctlkey    }, "o", function() useless_gaps_resize(0) end,
                {description = "focus next up", group = "client"}),

              -- bind slash key
              awful.key({ modkey, }, "#61", function() awful.spawn("'''${pkgs.bemenu}/bin/bemenu-run -w --fn 'mono 15' -p Run") end,
                { description = "run prompt", group = "launcher" }),
              -- Menubar
              awful.key({ modkey, sftkey }, "#61", function() menubar.show() end,
                {description = "show the menubar", group = "launcher"}),
              awful.key({ modkey, sftkey  }, "BackSpace",      hotkeys_popup.show_help,
                {description="show help", group="awesome"}),

              awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
                {description = "increase master width factor", group = "layout"}),
              awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
                {description = "decrease master width factor", group = "layout"}),
              awful.key({ modkey, sftkey   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
                {description = "increase the number of master clients", group = "layout"}),
              awful.key({ modkey, sftkey   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
                {description = "decrease the number of master clients", group = "layout"}),
              awful.key({ modkey, ctlkey }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
                {description = "increase the number of columns", group = "layout"}),
              awful.key({ modkey, ctlkey }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
                {description = "decrease the number of columns", group = "layout"}),


              awful.key({ modkey }, "Escape", function ()
                awful.spawn("${inputs.self.packages.${pkgs.system}.sysact}/bin/sysact")
                end,
                {description = "sys menu", group = "applications"}),

              awful.key({ modkey }, "x",
                function ()
                awful.spawn("${locker}")
                end,
                {description = "lua execute prompt", group = "awesome"}),


              awful.key({ }, "XF86MonBrightnessDown", function ()
                awful.spawn("${brightnessctl} s 5%-")
                end,
                {description = "decrease brightness", group = "laptop"}),
              awful.key({ }, "XF86MonBrightnessUp", function ()
                awful.spawn("${brightnessctl} s 5%+")
                end,
                {description = "increase brightness", group = "laptop"}),

              awful.key({ }, "XF86AudioRaiseVolume", function ()
                awful.spawn("wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 1%+")
                awesome.emit_signal("volume::update")
                end,
                {description = "increase volume", group = "media"}),
              awful.key({ modkey, }, "equal", function ()
                awful.spawn("wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 1%+")
                awesome.emit_signal("volume::update")
                end,
                {description = "increase volume", group = "media"}),

              awful.key({ }, "XF86AudioLowerVolume", function ()
                awful.spawn("wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 1%-")
                awesome.emit_signal("volume::update")
                end,
                {description = "decrease volume", group = "media"}),
              awful.key({ modkey, }, "minus", function ()
                awful.spawn("wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 1%-")
                awesome.emit_signal("volume::update")
                end,
                {description = "decrease volume", group = "media"}),

              awful.key({ }, "XF86AudioMute", function ()
                awful.spawn("wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle")
                awesome.emit_signal("volume::update")
                end,
                {description = "mute volume", group = "media"}),
              awful.key({ modkey, }, "m", function ()
                awful.spawn("wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle")
                awesome.emit_signal("volume::update")
                end,
                {description = "mute volume", group = "media"}),

              awful.key({ modkey, sftkey }, "m", function ()
                awful.spawn(terminal .. " -e ${pkgs.ncmpcpp}/bin/ncmpcpp")
                end,
                {description = "sys menu", group = "applications"}),

              awful.key({ modkey, }, "p", function ()
                awful.spawn.easy_async_with_shell("'''${dmc} -P", function(stdout)
                  awesome.emit_signal("music::update")
                end)
                end,
                {description = "pause/play", group = "media"}),
              awful.key({ }, "XF86AudioPlay", function ()
                awful.spawn.easy_async_with_shell("'''${dmc} -P", function(stdout)
                  awesome.emit_signal("music::update")
                end)
                end,
                {description = "pause/play", group = "media"}),
              awful.key({ }, "XF86AudioPause", function ()
                awful.spawn.easy_async_with_shell("'''${dmc} -S", function(stdout)
                  awesome.emit_signal("music::update")
                end)
                end,
                {description = "stop play", group = "media"}),
              awful.key({ modkey, }, "bracketright", function ()
                awful.spawn("'''${dmc} -f 5")
                end,
                {description = "fast forward", group = "media"}),
              awful.key({ modkey, sftkey }, "bracketright", function ()
                awful.spawn("'''${dmc} -f 10")
                end,
                {description = "fast forward", group = "media"}),
              awful.key({ modkey, }, "bracketleft", function ()
                awful.spawn("'''${dmc} -b 5")
                end,
                {description = "fast backward", group = "media"}),
              awful.key({ modkey, sftkey }, "bracketleft", function ()
                awful.spawn("'''${dmc} -b 10")
                end,
                {description = "fast backward", group = "media"}),
              awful.key({ modkey, }, "comma", function ()
                awful.spawn.easy_async_with_shell("'''${dmc} -p", function(stdout)
                  awesome.emit_signal("music::update")
                end)
                end,
                {description = "prev song", group = "media"}),
              awful.key({ }, "XF86AudioPrev", function ()
                awful.spawn.easy_async_with_shell("'''${dmc} -p", function(stdout)
                  awesome.emit_signal("music::update")
                end)
                end,
                {description = "prev song", group = "media"}),
              awful.key({ modkey, }, "period", function ()
                awful.spawn.easy_async_with_shell("'''${dmc} -n", function(stdout)
                  awesome.emit_signal("music::update")
                end)
                end,
                {description = "prev song", group = "media"}),
              awful.key({ }, "XF86AudioNext", function ()
                awful.spawn.easy_async_with_shell("'''${dmc} -n", function(stdout)
                  awesome.emit_signal("music::update")
                end)
                end,
                {description = "prev song", group = "media"}),

              awful.key({ modkey, sftkey }, "p", function ()
                awful.spawn("'''${pkgs.playerctl}/bin/playerctl play-pause")
                end,
                {description = "toggle mpris", group = "media"}),

              awful.key({ modkey, sftkey, ctlkey }, "p", function ()
                awful.spawn("'''${pkgs.playerctl}/bin/playerctl pause")
                end,
                {description = "pause mpris", group = "media"}),

              awful.key({ modkey, sftkey }, "comma", function ()
                awful.spawn("'''${pkgs.playerctl}/bin/playerctl shift")
                end,
                {description = "next mpris player", group = "media"}),

              awful.key({ modkey, sftkey }, "period", function ()
                awful.spawn("'''${pkgs.playerctl}/bin/playerctl unshift")
                end,
                {description = "prev mpris player", group = "media"}),

              awful.key({ }, "Print", function ()
                awful.spawn("'''${maimpick} -f")
                end,
                {description = "screenshot", group = "applications"}),
              awful.key({ modkey }, "Print", function ()
                awful.spawn("'''${maimpick}")
                end,
                {description = "screenshot", group = "applications"}),
              awful.key({ ctlkey }, "Print", function ()
                awful.spawn("'''${maimpick} -a")
                end,
                {description = "screenshot", group = "applications"}),
              awful.key({ altkey }, "Print", function ()
                awful.spawn("'''${maimpick} -w")
                end,
                {description = "screenshot", group = "applications"}),


              awful.key({ modkey, }, "e", function ()
                awful.spawn("'''${pkgs.xfce.thunar}/bin/thunar")
                end,
                {description = "files", group = "applications"}),

              awful.key({ modkey, }, "c", function ()
                awful.spawn("'''${nmctl} -c")
                end,
                {description = "net connect", group = "applications"}),

              awful.key({ modkey, ctlkey }, "n",
                function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                    c:emit_signal(
                      "request::activate", "key.unminimize", {raise = true}
                      )
                  end
                end,
                {description = "restore minimized", group = "client"}),

              -- Prompt
              awful.key({ modkey }, "r", function () awful.screen.focused().mypromptbox:run() end,
                {description = "run prompt", group = "launcher"}),

              awful.key({ modkey, sftkey }, "x",
                function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
                end,
                {description = "lua execute prompt", group = "awesome"})
          )

            clientkeys = gears.table.join(
                -- Move to screen
                awful.key({ modkey, sftkey }, "Right",
                  function(c) c:move_to_screen(c.screen.index - 1) end,
                  { description = "Move to screen", group = "client" }),
                awful.key({ modkey, sftkey }, "Left",
                  function(c) c:move_to_screen(c.screen.index + 1) end,
                  { description = "Move to screen", group = "client" }),
                awful.key({ modkey, sftkey }, "h",
                  function(c) c:move_to_screen(c.screen.index + 1) end,
                  { description = "Move to screen", group = "client" }),
                awful.key({ modkey, sftkey }, "l",
                  function(c) c:move_to_screen(c.screen.index - 1) end,
                  { description = "Move to screen", group = "client" }),

                -- resize clients
                awful.key({ modkey, altkey }, "Down",
                  function(c)
                    if c.floating then
                      c:relative_move(0, 0, 0, 10)
                    else
                      awful.client.incwfact(-0.05)
                    end
                  end,
                  { description = "increase client height", group = "client" }),
                awful.key({ modkey, altkey }, "Right",
                  function(c)
                    if c.floating then
                      c:relative_move(0, 0, 10, 0)
                    else
                      awful.tag.incmwfact(0.05)
                    end
                  end,
                { description = "increase master width", group = "client" }),
                awful.key({ modkey, altkey }, "Left",
                  function(c)
                    if c.floating then
                      c:relative_move(0, 0, -10, 0)
                    else
                      awful.tag.incmwfact(-0.05)
                    end
                  end,
                { description = "decrease master width", group = "client" }),
                awful.key({ modkey, altkey }, "Up",
                  function(c)
                    if c.floating then
                      c:relative_move(0, 0, 0, -10)
                    else
                      awful.client.incwfact(0.05)
                    end
                  end,
                { description = "decrease client height", group = "client" }),

                awful.key({ modkey,           }, "f",
                  function (c)
                    c.fullscreen = not c.fullscreen
                    c:raise()
                  end,
                  {description = "toggle fullscreen", group = "client"}),

                awful.key({ modkey, sftkey }, "f",
                    function(c)
                      c.floating = not c.floating
                      c:raise()
                    end,
                  { description = "toggle floating", group = "client" }),

                awful.key({ modkey, sftkey }, "b",
                    function()
                        for _, c in ipairs(client.get()) do
                            awful.titlebar.toggle(c)
                        end
                    end,
                    { description = "toggle titlebar", group = "client" }),

                awful.key({ modkey, }, "space",
                    function(c)
                        c:swap(awful.client.getmaster())
                    end,
                    { description = "move to master", group = "client" }),

                awful.key({ modkey, }, "q", function (c) c:kill() end,
                  {description = "close", group = "client"})
          )

            -- Bind all key numbers to tags.
            -- Be careful: we use keycodes to make it work on any keyboard layout.
            -- 1 keycode is 10 and F1 keycode is 67
            -- This should map on the top row of your keyboard, usually 1 to 9.
            for i, tag in ipairs(tags) do
              local keycode = i + 9
              if i > 10 then
                keycode = keycode + 47
              end
              globalkeys = gears.table.join(globalkeys,
                -- View tag only.
                awful.key({ modkey }, "#" .. keycode,
                  function ()
                    local screen = awful.screen.focused()
                    local tag = screen.tags[i]
                    if tag then
                      tag:view_only()
                    end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
                -- Toggle tag display.
                awful.key({ modkey, "Control" }, "#" .. keycode,
                  function ()
                    local screen = awful.screen.focused()
                    local tag = screen.tags[i]
                    if tag then
                      awful.tag.viewtoggle(tag)
                    end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
                -- Move client to tag.
                awful.key({ modkey, "Shift" }, "#" .. keycode,
                  function ()
                    if client.focus then
                      local tag = client.focus.screen.tags[i]
                      if tag then
                        client.focus:move_to_tag(tag)
                      end
                    end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
                -- Toggle tag on focused client.
                awful.key({ modkey, "Control", "Shift" }, "#" .. keycode,
                  function ()
                    if client.focus then
                      local tag = client.focus.screen.tags[i]
                      if tag then
                        client.focus:toggle_tag(tag)
                      end
                    end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
              )
            end

            clientbuttons = gears.table.join(
                awful.button({ }, 1, function (c)
                  c:emit_signal("request::activate", "mouse_click", {raise = true})
                  end),
                awful.button({ modkey }, 1, function (c)
                  c:emit_signal("request::activate", "mouse_click", {raise = true})
                  awful.mouse.client.move(c)
                  end),
                awful.button({ modkey }, 3, function (c)
                  c:emit_signal("request::activate", "mouse_click", {raise = true})
                  awful.mouse.client.resize(c)
                  end)
                )

            -- Set keys
            root.keys(globalkeys)
            -- }}}

        -- {{{ Rules
          -- Rules to apply to new clients (through the "manage" signal).
            awful.rules.rules = {
              -- All clients will match this rule.
              { rule = { },
                properties = {
                  border_width = beautiful.border_width,
                  border_color = beautiful.border_normal,
                  focus = awful.client.focus.filter,
                  raise = true,
                  maximized = false,
                  opacity = 1,
                  keys = clientkeys,
                  buttons = clientbuttons,
                  screen = awful.screen.preferred,
                  placement = awful.placement.no_overlap+awful.placement.no_offscreen
                }
              },

              -- Floating clients.
              {
                rule_any = {
                  instance = {
                    "DTA",  -- Firefox addon DownThemAll.
                    "copyq",  -- Includes session name in class.
                    "pinentry",
                  },
                  class = {
                    "Arandr",
                    "Blueman-manager",
                    "Gpick",
                    "Kruler",
                    "MessageWin",  -- kalarm.
                    "Sxiv",
                    "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
                    "Wpa_gui",
                    "veromix",
                    "xtightvncviewer"
                  },

                  -- Note that the name property shown in xprop might be set slightly after creation of the client
                  -- and the name shown there might not match defined rules here.
                  name = {
                    "Event Tester",  -- xev.
                  },
                  role = {
                    "AlarmWindow",  -- Thunderbird's calendar.
                    "ConfigManager",  -- Thunderbird's about:config.
                    "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
                  }
                },
                properties = {
                  floating = true,
                  placement = awful.placement.centered,
                }
              },

              -- Add titlebars to normal clients and dialogs
              {
                rule_any = {
                  type = { "normal", "dialog" }
                },
                properties = { titlebars_enabled = true }
              },
              {
                rule = { instance = scratchpad_name .. "*" },
                properties = {
                  width = awful.screen.focused().workarea.width * 0.8,
                  height = awful.screen.focused().workarea.height * 0.8,
                  skip_taskbar = true,
                  floating  = true,
                  hidden = true,
                  sticky = true,
                  new_tag = TAG_SP,
                  placement = awful.placement.centered,
                },
                callback = function (c)
                  awful.placement.centered(c,{honor_padding = true, honor_workarea=true})
                  gears.timer.delayed_call(function()
                      c.urgent = false
                      end)
                  end
              },
              {
                rule_any = {
                  class = {"Firefox", "Brave-browser"},
                  role = { "browser-window" }
                },
                properties = { opacity = 1, maximized = false, floating = false }
              },
              {
                rule = { class = 'XVkbd' },
                properties = {
                  floating  = true,
                  titlebars_enabled = true,
                  sticky = true
                }
              },
            }
          -- }}}

        -- {{{ Signals

          --Toggle titlebar on floating status change
          client.connect_signal("property::floating", function(c)
           setTitlebar(c, c.floating)
          end)

          -- Signal function to execute when a new client appears.
          client.connect_signal("manage", function (c)
            -- Set the windows at the slave,
            -- i.e. put it at the end of others instead of setting it master.
            -- if not awesome.startup then awful.client.setslave(c) end

            setTitlebar(c, c.floating or c.first_tag.layout == awful.layout.suit.floating)

            if awesome.startup
              and not c.size_hints.user_position
              and not c.size_hints.program_position then
                -- Prevent clients from being unreachable after screen count changes.
                awful.placement.no_offscreen(c)
            end
          end)

          -- Show titlebars on tags with the floating layout
          tag.connect_signal("property::layout", function(t)
            -- New to Lua ?
            -- pairs iterates on the table and return a key value pair
            -- I don't need the key here, so I put _ to ignore it
            for _, c in pairs(t:clients()) do
              if t.layout == awful.layout.suit.floating then
                setTitlebar(c, true)
              else
                setTitlebar(c, false)
              end
            end
          end)

          -- Handle screen being removed.
          -- We'll look for same tag names and move clients there, but preserve
          -- the "desired_screen" so we can move them back when it's connected.
          tag.connect_signal("request::screen", function(t)
            local fallback_tag = nil

            -- find tag with same name on any other screen
            for other_screen in screen do
              if other_screen ~= t.screen then
                fallback_tag = awful.tag.find_by_name(other_screen, t.name)
                if fallback_tag ~= nil then
                  break
                end
              end
            end

            -- no tag with same name exists, use fallback
            if fallback_tag == nil then
              fallback_tag = awful.tag.find_fallback()
            end

            if not (fallback_tag == nil) then
              clients = t:clients()
              for _, c in ipairs(clients) do
                c:move_to_tag(fallback_tag)
                -- preserve info about which screen the window was originally on, so
                -- we can restore it if the screen is reconnected.
                c.desired_screen = t.screen.index
              end
            end
          end)

          -- Add a titlebar if titlebars_enabled is set to true in the rules.
          client.connect_signal("request::titlebars", function(c)
            -- buttons for the titlebar
            local buttons = gears.table.join(
              awful.button({ }, 1, function()
                c:emit_signal("request::activate", "titlebar", {raise = true})
                awful.mouse.client.move(c)
              end),
              awful.button({ }, 3, function()
                c:emit_signal("request::activate", "titlebar", {raise = true})
                awful.mouse.client.resize(c)
              end)
            )

            awful.titlebar(c) : setup {
              { -- Left
              awful.titlebar.widget.iconwidget(c),
              buttons = buttons,
              layout  = wibox.layout.fixed.horizontal
              },
              { -- Middle
              { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
              },
              buttons = buttons,
              layout  = wibox.layout.flex.horizontal
              },
              { -- Right
                awful.titlebar.widget.floatingbutton (c),
                awful.titlebar.widget.maximizedbutton(c),
                awful.titlebar.widget.stickybutton   (c),
                awful.titlebar.widget.ontopbutton    (c),
                awful.titlebar.widget.closebutton    (c),
                layout = wibox.layout.fixed.horizontal()
              },
              layout = wibox.layout.align.horizontal
            }
          end)

          -- Enable sloppy focus, so that focus follows mouse.
          client.connect_signal("mouse::enter", function(c)
            c:emit_signal("request::activate", "mouse_enter", {raise = false})
          end)

          client.connect_signal("property::fullscreen", function(c)
            local s = awful.screen.focused()
            if s ~= nil then
              s.myTopWibox.visible = not c.fullscreen
              s.myBottomWibox.visible = not c.fullscreen
            end
            -- Calculate the wibox bars for fullscreen
            -- for s in screen do
            --   s.myTopWibox.visible = not c.fullscreen
            --   s.myBottomWibox.visible = not c.fullscreen
            -- end

            -- disable transparentcy on fullscreen
            -- if c.fullscreen then
            --   awesome.spawn("killall xcompmgr")
            -- else
            --   awesome.spawn("xcompmgr")
            -- end
          end)

          client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
          client.connect_signal("unfocus", function(c)
            c.border_color = beautiful.border_normal

            if c.fullscreen then
              for s in screen do
                s.myTopWibox.visible = true
                s.myBottomWibox.visible = true
              end
            end
          end)
            -- }}}
      '';
  };
}
