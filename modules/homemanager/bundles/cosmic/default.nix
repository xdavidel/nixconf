{
  lib,
  config,
  pkgs,
  inputs,
  ...
}: {
  imports = [
    ./shortcuts.nix
  ];

  mHomeManager.bundles = {
    wayland.enable = true;
  };

  home.file.".config/cosmic/com.system76.CosmicComp/v1/xkb_config" = {
    force = true;
    text = ''
      (
        rules: "",
        model: "pc104",
        layout: "us,il",
        variant: "",
        options: Some("grp:alt_shift_toggle,caps:escape"),
        repeat_delay: 300,
        repeat_rate: 50,
      )
    '';
  };

  home.file.".config/cosmic/com.system76.CosmicPanel.Dock/" = {
    force = true;
    source = ./CosmicPanel.Dock;
    recursive = true;
  };

  home.file.".config/cosmic/com.system76.CosmicPanel.Panel/" = {
    force = true;
    source = ./CosmicPanel.Panel;
    recursive = true;
  };
}
