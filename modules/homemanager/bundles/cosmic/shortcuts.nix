{
  lib,
  config,
  pkgs,
  inputs,
  ...
}: {
  home.file.".config/cosmic/com.system76.CosmicSettings.Shortcuts/v1/custom" = let
    terminal = config.home.sessionVariables.TERMINAL;
    dmc = "${inputs.self.packages.${pkgs.system}.dmc}/bin/dmc";
  in
    lib.mkIf (config.mHomeManager.bundles.cosmic.enable) {
      force = true;
      text = ''
        {
            (
                modifiers: [
                    Super,
                ],
                key: "e",
            ): System(HomeFolder),
            (
                modifiers: [
                    Super,
                ],
                key: "comma",
            ): System(PlayPrev),
            (
                modifiers: [
                    Super,
                ],
                key: "period",
            ): System(PlayNext),
            (
                modifiers: [
                    Super,
                    Ctrl,
                ],
                key: "Down",
            ): NextOutput,
            (
                modifiers: [
                    Super,
                    Ctrl,
                    Alt,
                ],
                key: "k",
            ): Disable,
            (
                modifiers: [
                    Super,
                ],
                key: "t",
            ): ToggleTiling,
            (
                modifiers: [
                    Super,
                    Ctrl,
                    Alt,
                ],
                key: "h",
            ): Disable,
            (
                modifiers: [
                    Super,
                    Ctrl,
                ],
                key: "Right",
            ): NextWorkspace,
            (
                modifiers: [
                    Super,
                ],
                key: "m",
            ): System(Mute),
            (
                modifiers: [
                    Super,
                    Ctrl,
                ],
                key: "Up",
            ): PreviousOutput,
            (
                modifiers: [
                    Super,
                    Shift,
                ],
                key: "comma",
            ): Disable,
            (
                modifiers: [
                    Super,
                    Ctrl,
                    Alt,
                ],
                key: "j",
            ): Disable,
            (
                modifiers: [
                    Super,
                ],
                key: "y",
            ): ToggleSticky,
            (
                modifiers: [
                    Super,
                    Shift,
                ],
                key: "f",
            ): ToggleWindowFloating,
            (
                modifiers: [
                    Super,
                ],
                key: "equal",
            ): System(VolumeRaise),
            (
                modifiers: [
                    Super,
                    Shift,
                ],
                key: "period",
            ): Disable,
            (
                modifiers: [
                    Super,
                    Ctrl,
                    Alt,
                ],
                key: "Left",
            ): Disable,
            (
                modifiers: [
                    Super,
                ],
                key: "minus",
            ): System(VolumeLower),
            (
                modifiers: [
                    Super,
                ],
                key: "g",
            ): Disable,
            (
                modifiers: [
                    Super,
                    Ctrl,
                    Alt,
                ],
                key: "l",
            ): Disable,
            (
                modifiers: [
                    Super,
                ],
            ): Disable,
            (
                modifiers: [
                    Super,
                    Ctrl,
                    Alt,
                ],
                key: "Right",
            ): Disable,
            (
                modifiers: [
                    Super,
                ],
                key: "f",
            ): Maximize,
            (
                modifiers: [
                    Super,
                    Ctrl,
                ],
                key: "Left",
            ): PreviousWorkspace,
            (
                modifiers: [
                    Super,
                    Ctrl,
                    Alt,
                ],
                key: "Down",
            ): Disable,
            (
                modifiers: [
                    Super,
                    Ctrl,
                    Alt,
                ],
                key: "Up",
            ): Disable,
            (
                modifiers: [
                    Super,
                    Shift,
                ],
                key: "p",
            ): System(PlayPause),
            (
                modifiers: [
                    Super,
                    Shift,
                ],
                key: "Return",
            ): System(Terminal),
            (
                modifiers: [
                    Super,
                ],
                key: "Return",
                description: Some("Terminal"),
            ): Spawn("${terminal}"),
            (
                modifiers: [
                    Super,
                ],
                key: "comma",
                description: Some("Prev Audio Source"),
            ): Spawn("${dmc} -p"),
            (
                modifiers: [
                    Super,
                ],
                key: "period",
                description: Some("Next Audio Source"),
            ): Spawn("${dmc} -n"),
            (
                modifiers: [
                    Super,
                    Ctrl,
                ],
                key: "comma",
                description: Some("Prev Audio Source"),
            ): Spawn("${pkgs.playerctl}/bin/playerctld unshift"),
            (
                modifiers: [
                    Super,
                    Ctrl,
                ],
                key: "period",
                description: Some("Next Audio Source"),
            ): Spawn("${pkgs.playerctl}/bin/playerctld shift"),
            (
                modifiers: [
                    Super,
                ],
                key: "p",
                description: Some("Music Play/Pause"),
            ): Spawn("${dmc} -P"),
            (
                modifiers: [
                    Super,
                ],
                key: "Bracketleft",
                description: Some("Music Seek Backward"),
            ): Spawn("${dmc} -b 5"),
            (
                modifiers: [
                    Super,
                ],
                key: "Bracketright",
                description: Some("Music Seek Forward"),
            ): Spawn("${dmc} -f 5"),
            (
                modifiers: [
                    Super,
                    Shift,
                ],
                key: "Bracketleft",
                description: Some("Music Seek Backward Long"),
            ): Spawn("${dmc} -b 10"),
            (
                modifiers: [
                    Super,
                    Shift,
                ],
                key: "Bracketright",
                description: Some("Music Seek Forward Long"),
            ): Spawn("${dmc} -f 10"),
        }
      '';
    };
}
