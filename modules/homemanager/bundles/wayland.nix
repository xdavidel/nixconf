{
  config,
  lib,
  pkgs,
  inputs,
  ...
}: {
  programs.imv = {
    enable = true;
    settings = {
      binds = {
        w = "exec ${inputs.self.packages.${pkgs.system}.setbg}/bin/setbg $imv_current_file";
      };
    };
  };

  xdg.mimeApps.defaultApplications = let
    imv = "imv-dir.desktop";
  in {
    "image/svg+xml" = "${imv}";
    "image/png" = "${imv}";
    "image/jpeg" = "${imv}";
    "image/webp" = "${imv}";
    "image/gif" = "${imv}";
  };

  home.packages = with pkgs; [
    grim
    slurp
    swww
    wdisplays
    wf-recorder
    wl-clipboard
    wl-mirror
  ];
}
