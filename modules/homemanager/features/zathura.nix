{
  config,
  lib,
  pkgs,
  ...
}: {
  xdg.mimeApps.defaultApplications = let
    zathura = "org.pwmt.zathura.desktop";
  in {
    "application/postscript" = "${zathura}";
    "application/pdf" = "${zathura}";
  };

  programs.zathura = {
    enable = true;
    mappings = {
      u = "scroll half-up";
      d = "scroll half-down";
      D = "toggle_page_mode";
      r = "reload";
      R = "rotate";
      K = "zoom in";
      J = "zoom out";
      i = "recolor";
      p = "print";
    };
    options = {
      sandbox = "none";
      statusbar-h-padding = 0;
      statusbar-v-padding = 0;
      page-padding = 1;
      selection-clipboard = "clipboard";
    };
  };
}
