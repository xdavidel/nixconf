{
  config,
  pkgs,
  lib,
  ...
}: let
  helixpkg = pkgs.unstable.evil-helix;
in {
  options = {
    mHomeManager.helix.default = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = "Set Helix as the default editor";
    };
  };

  programs.helix = {
    enable = true;
    package = helixpkg;
    defaultEditor = config.mHomeManager.helix.default;
    extraPackages = with pkgs; [
      clang-tools
      lldb
    ];
    settings = {
      theme = "catppuccin_mocha";
      editor = {
        cursor-shape = {
          insert = "bar";
        };
      };
      keys.normal = {
        K = "hover";
        V = ["extend_line_below" "select_mode"];
        Z = {
          Z = ":write-quit-all";
          Q = ":quit!";
        };
      };
    };
  };
}
