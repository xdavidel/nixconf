{
  config,
  pkgs,
  inputs,
  ...
}: let
  neovim = inputs.nvim.packages.${pkgs.system}.default;
in {
  home.packages = let
    nvim-desktop = pkgs.makeDesktopItem {
      name = "nvim-desktop";
      desktopName = "Neovim Desktop";
      genericName = "Text Editor";
      exec = ''${config.home.sessionVariables.TERMINAL} -e ${neovim}/bin/nvim %u'';
      icon = "nvim";
      categories = ["Utility" "TextEditor"];
      mimeTypes = [
        "text/plain"
        "text/x-shellscript"
        "text/english"
        "text/x-makefile"
        "text/x-c++hdr"
        "text/x-c++src "
        "text/x-chdr"
        "text/x-csrc"
        "text/x-java"
        "text/x-moc"
        "text/x-pascal"
        "text/x-tcl"
        "text/x-tex"
        "text/x-c"
        "text/x-c++"
      ];
    };
  in
    [
      neovim
    ]
    ++ (
      if (config.graphical.option == true)
      then [nvim-desktop]
      else []
    );

  programs.neovim = {
    package = neovim;
    viAlias = true;
    vimAlias = true;
    vimdiffAlias = true;
    defaultEditor = true;
  };

  home.sessionVariables = {
    EDITOR = "nvim";
  };

  xdg.mimeApps.defaultApplications = let
    nvim = "nvim-desktop.desktop";
  in {
    "text/plain" = "${nvim}";
    "text/x-shellscript" = "${nvim}";
  };
}
