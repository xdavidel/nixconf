{
  config,
  lib,
  pkgs,
  ...
}: {
  xdg.mimeApps.defaultApplications = let
    evince = "org.gnome.Evince.desktop";
  in {
    "application/postscript" = "${evince}";
    "application/pdf" = "${evince}";
  };

  home.packages = with pkgs; [
    evince
  ];
}
