{
  config,
  pkgs,
  lib,
  ...
}: let
  zedpkg = pkgs.unstable.zed-editor;
  zed = "${zedpkg}/bin/zeditor";
in {
  options = {
    mHomeManager.zed.default = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = "Set Zed as the default editor";
    };
  };

  programs.zed-editor = {
    enable = true;
    package = zedpkg;
    userSettings = {
      theme = {
        mode = "dark";
        dark = "Catppuccin Mocha (Blur)";
        light = "One Light";
      };
      features = {
        copilot = false;
      };
      telemetry = {
        diagnostics = false;
        metrics = false;
      };
      vim_mode = true;
      ui_font_size = 16;
      buffer_font_size = 16;
      cursor_blink = false;
    };
    extensions = ["zig" "nix" "catppuccin-blur"];
  };

  home = {
    sessionVariables = lib.mkIf (config.mHomeManager.zed.default) {
      EDITOR = zed;
    };
  };
}
