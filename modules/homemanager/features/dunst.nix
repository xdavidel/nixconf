{
  config,
  pkgs,
  ...
}: {
  services.dunst = let
    inherit (config.colorscheme) colors;
    msgbox = "${pkgs.papirus-icon-theme}/share/icons/Papirus/48x48/status/messagebox_";
  in {
    enable = true;
    settings = {
      global = {
        alignment = "left";
        corner_radius = 10;
        corners = "bottom, top-left";
        ellipsize = "middle";
        follow = "mouse";
        font = "Mono 12";
        format = "<b>%s</b>\\n%b";
        frame_color = "#eceff1";
        frame_width = 2;
        gap_size = 6;
        height = "(0, 300)";
        icon_position = "right";
        ignore_newline = "no";
        line_height = 0;
        markup = "full";
        mouse_left_click = "do_action";
        mouse_middle_click = "close_all";
        mouse_right_click = "close_current";
        notification_height = 0;
        offset = "(5, 15)";
        origin = "top-right";
        padding = 8;
        progress_bar = true;
        progress_bar_corner_radius = 50;
        progress_bar_corners = "bottom-left, top-right";
        separator_color = "auto";
        separator_height = 1;
        sort = "yes";
        transparency = 15;
        vertical_alignment = "center";
        width = "(300, 600)";
        word_wrap = "yes";
      };
      urgency_low = {
        background = "#${colors.base01}";
        foreground = "#${colors.base05}";
        frame_color = "#${colors.base00}";
        timeout = 5;
      };

      urgency_normal = {
        background = "#${colors.base01}";
        foreground = "#${colors.base05}";
        frame_color = "#${colors.base0E}";
        highlight = "#${colors.base0E}, #${colors.base0A}";
        timeout = 5;
        default_icon = "${msgbox}info.svg";
      };

      urgency_critical = {
        background = "#${colors.base01}";
        foreground = "#${colors.base05}";
        frame_color = "#${colors.base08}";
        highlight = "#${colors.base08}, #${colors.base06}";
        default_icon = "${msgbox}critical.svg";
      };
    };
  };
}
