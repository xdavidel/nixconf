{
  config,
  pkgs,
  inputs,
  lib,
  ...
}: let
  likecritty = inputs.self.packages.${pkgs.system}.likecritty;
  likecritty-bin = "${likecritty}/bin/likecritty";
in {
  options = {
    mHomeManager.alacritty.default = lib.mkOption {
      type = lib.types.bool;
      default = false;
    };
  };

  config = {
    programs.alacritty = let
      inherit (config.colorscheme) colors;
    in {
      enable = true;
      settings = {
        env.TERM = "xterm-256color";
        selection.save_to_clipboard = true;
        window = {
          padding = {
            x = 5;
            y = 5;
          };
          opacity = 0.9;

          class = {
            instance = "Alacritty";
            general = "Alacritty";
          };
        };
        scrolling.history = 10000;
        cursor = {
          style.shape = "Beam";
          vi_mode_style.shape = "Block";
        };
        mouse.bindings = [
          {
            mouse = "Left";
            action = "Copy";
          }
          {
            mouse = "Right";
            action = "PasteSelection";
          }
        ];
        keyboard.bindings = [
          {
            key = "V";
            mods = "Control|Shift";
            action = "Paste";
          }
          {
            key = "C";
            mods = "Control|Shift";
            action = "Copy";
          }
          {
            key = "`";
            mods = "Control";
            chars = "\\u001B[96;5u";
          }
          {
            key = "N";
            mods = "Control|Shift";
            action = "SpawnNewInstance";
          }
          {
            key = "F";
            mods = "Control|Shift";
            chars = "\\u001B[70;5u";
          }
          {
            key = "Equals";
            mods = "Control";
            action = "IncreaseFontSize";
          }
          {
            key = "Plus";
            mods = "Control";
            action = "IncreaseFontSize";
          }
          {
            key = "Minus";
            mods = "Control";
            action = "DecreaseFontSize";
          }
          {
            key = "Key0";
            mods = "Control";
            action = "ResetFontSize";
          }
          {
            key = "Insert";
            mods = "Shift";
            action = "PasteSelection";
          }
        ];
        colors = {
          cursor = {
            cursor = "#ad7fa8";
            text = "CellBackground";
          };

          primary = {
            background = "#${colors.base00}";
            foreground = "#${colors.base05}";
          };
          normal = {
            black = "#${colors.base01}";
            white = "#${colors.base06}";
            red = "#${colors.base08}";
            yellow = "#${colors.base0A}";
            green = "#${colors.base0B}";
            cyan = "#${colors.base0C}";
            blue = "#${colors.base0D}";
            magenta = "#${colors.base0E}";
          };

          search = {
            focused_match = {
              background = "#ff2222";
              foreground = "#ffffff";
            };
            matches = {
              background = "#ef8171";
              foreground = "#000000";
            };
          };

          vi_mode_cursor = {
            cursor = "#fff796";
            text = "CellBackground";
          };
        };
      };
    };

    home.packages = [
      likecritty
    ];

    home.sessionVariables = lib.mkIf (config.mHomeManager.alacritty.default) {
      TERMINAL = "${likecritty-bin}";
    };

    home.file.".config/Thunar/uca.xml" = let
      file-roller = "${pkgs.file-roller}/bin/file-roller";
    in
      lib.mkIf (config.mHomeManager.alacritty.default) {
        text = ''
          <?xml version="1.0" encoding="UTF-8"?>
          <actions>
          <action>
          <icon>utilities-terminal</icon>
          <name>Open Terminal Here</name>
          <submenu></submenu>
          <unique-id>1704934536132743-1</unique-id>
          <command>${likecritty-bin} --working-directory %f</command>
          <description>Example for a custom action</description>
          <range></range>
          <patterns>*</patterns>
          <startup-notify/>
          <directories/>
          </action>
          <action>
          <icon></icon>
          <name>Compress...</name>
          <submenu></submenu>
          <unique-id>1717492753294488-1</unique-id>
          <command>${file-roller} -d %F</command>
          <description>Compress selected</description>
          <range>*</range>
          <patterns>*</patterns>
          <directories/>
          <audio-files/>
          <image-files/>
          <other-files/>
          <text-files/>
          <video-files/>
          </action>
          <action>
          <icon></icon>
          <name>Extract...</name>
          <submenu></submenu>
          <unique-id>1717493510995203-2</unique-id>
          <command>${file-roller} -f %f</command>
          <description></description>
          <range>*</range>
          <patterns>*.zip;*.7z;*.tar;*.tar.gz;*.rar</patterns>
          <other-files/>
          </action>
          </actions>
        '';
        force = true;
      };
  };
}
