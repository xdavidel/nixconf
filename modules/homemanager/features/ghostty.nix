{
  config,
  pkgs,
  lib,
  ...
}: let
  ghostty = pkgs.unstable.ghostty;
  term = "${ghostty}/bin/ghostty";
in {
  options = {
    mHomeManager.ghostty.default = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = "Set ghostty as the default terminal";
    };
  };

  home.file.".config/ghostty/config" = {
    text =
      /*
      conf
      */
      ''
        background-opacity = 0.9
        confirm-close-surface = false
        copy-on-select = clipboard
        cursor-style-blink = false
        focus-follows-mouse = true
        gtk-single-instance = true
        mouse-hide-while-typing = true
        shell-integration-features = no-cursor,sudo,title
        theme = catppuccin-mocha
        window-decoration = false
        window-padding-x = 3
        window-padding-y = 3

        keybind = ctrl+shift+h=write_scrollback_file:open
        keybind = ctrl+alt+t=toggle_tab_overview
      '';
    force = true;
  };

  home.packages = [
    ghostty
  ];

  home = {
    sessionVariables = lib.mkIf (config.mHomeManager.ghostty.default) {
      TERMINAL = term;
    };
  };

  home.file.".config/Thunar/uca.xml" = let
    file-roller = "${pkgs.file-roller}/bin/file-roller";
  in
    lib.mkIf (config.mHomeManager.ghostty.default) {
      text = ''
        <?xml version="1.0" encoding="UTF-8"?>
        <actions>
        <action>
        <icon>utilities-terminal</icon>
        <name>Open Terminal Here</name>
        <submenu></submenu>
        <unique-id>1704934536132743-1</unique-id>
        <command>${term} --working-directory %f</command>
        <description>Example for a custom action</description>
        <range></range>
        <patterns>*</patterns>
        <startup-notify/>
        <directories/>
        </action>
        <action>
        <icon></icon>
        <name>Compress...</name>
        <submenu></submenu>
        <unique-id>1717492753294488-1</unique-id>
        <command>${file-roller} -d %F</command>
        <description>Compress selected</description>
        <range>*</range>
        <patterns>*</patterns>
        <directories/>
        <audio-files/>
        <image-files/>
        <other-files/>
        <text-files/>
        <video-files/>
        </action>
        <action>
        <icon></icon>
        <name>Extract...</name>
        <submenu></submenu>
        <unique-id>1717493510995203-2</unique-id>
        <command>${file-roller} -f %f</command>
        <description></description>
        <range>*</range>
        <patterns>*.zip;*.7z;*.tar;*.tar.gz;*.rar</patterns>
        <other-files/>
        </action>
        </actions>
      '';
      force = true;
    };
}
