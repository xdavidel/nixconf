{
  config,
  lib,
  pkgs,
  ...
}: {
  xdg.mimeApps.defaultApplications = let
    brave = "brave-browser.desktop";
  in {
    "text/html" = "${brave}";
    "x-scheme-handler/http" = "${brave}";
    "x-scheme-handler/https" = "${brave}";
    "x-scheme-handler/about" = "${brave}";
    "x-scheme-handler/unknown" = "${brave}";
  };

  home.packages = with pkgs.unstable; [
    brave
  ];
}
