{
  config,
  lib,
  pkgs,
  ...
}: let
  kitty = pkgs.kitty;
  term = "${kitty}/bin/kitty --single-instance";
in {
  options = {
    mHomeManager.kitty.default = lib.mkOption {
      type = lib.types.bool;
      default = false;
    };
  };

  config = let
    inherit (config.colorscheme) colors;
    vi-mode-path = ".config/kitty/vi-mode.lua";
    homedir = config.home.homeDirectory;
  in {
    programs.kitty = {
      enable = true;
      package = kitty;
      settings = {
        scrollback_lines = 10000;
        enable_audio_bell = false;
        update_check_interval = 0;
        font_family = "monospace";
        bold_font = "auto";
        italic_font = "auto";
        bold_italic_font = "auto";

        background = "#${colors.base00}";
        foreground = "#${colors.base05}";

        # black
        color0 = "#${colors.base01}";
        color8 = "#${colors.base04}";

        # red
        color1 = "#${colors.base08}";
        color9 = "#${colors.base09}";

        # green
        color2 = "#${colors.base0B}";
        color10 = "#${colors.base0C}";

        # yellow
        color3 = "#${colors.base0A}";
        color11 = "#${colors.base0A}";

        # blue
        color4 = "#${colors.base0D}";
        color12 = "#${colors.base0C}";

        # magenta
        color5 = "#${colors.base0E}";
        color13 = "#${colors.base0F}";

        # cyan
        color6 = "#${colors.base0C}";
        color14 = "#${colors.base0D}";

        # white
        color7 = "#${colors.base06}";
        color15 = "#${colors.base05}";

        background_opacity = 0.9;

        cursor = "#ad7fa8";
        cursor_shape = "beam";
        cursor_shape_unfocused = "hollow";
        cursor_blink_interval = 0;

        cursor_trail = 1;
        cursor_trail_start_threshold = 2;

        # scrollback_pager = "${pkgs.bat}/bin/bat";
        scrollback_pager = ''nvim +"source ${homedir}/${vi-mode-path}"'';

        mouse_hide_wait = 3.0;

        copy_on_select = "clipboard";

        strip_trailing_spaces = "smart";

        confirm_os_window_close = 0;

        term = "xterm-256color"; # fix ssh
      };

      keybindings = {
        "ctrl+equal" = "change_font_size all +1.0";
        "ctrl+minus" = "change_font_size all -1.0";
        "ctrl+shift+n" = "launch --cwd=current --type=os-window";
        "ctrl+shift+space" = "show_scrollback";
      };

      extraConfig =
        /*
        conf
        */
        ''
          # unmap left click for open link (easy miss click)
          mouse_map left click ungrabbed no-op
          mouse_map ctrl+left click ungrabbed mouse_handle_click selection link prompt
          mouse_map ctrl+left press ungrabbed mouse_selection normal
        '';
    };

    home = {
      sessionVariables = lib.mkIf (config.mHomeManager.kitty.default) {
        TERMINAL = term;
      };
    };

    home.file.${vi-mode-path} = {
      force = true;
      text =
        /*
        lua
        */
        ''
          vim.wo.number = false
          vim.wo.relativenumber = false
          vim.wo.statuscolumn = ""
          vim.wo.signcolumn = "no"
          local orig_buf = vim.api.nvim_get_current_buf()
          local lines = vim.api.nvim_buf_get_lines(orig_buf, 0, -1, false)
          while #lines > 0 and vim.trim(lines[#lines]) == "" do
            lines[#lines] = nil
          end
          local buf = vim.api.nvim_create_buf(false, true)
          local channel = vim.api.nvim_open_term(buf, {})
          vim.api.nvim_chan_send(channel, table.concat(lines, "\r\n"))
          vim.api.nvim_set_current_buf(buf)
          vim.keymap.set("n", "q", "<cmd>qa!<cr>", { silent = true, buffer = buf })
          vim.api.nvim_create_autocmd("TermEnter", { buffer = buf, command = "stopinsert" })
          vim.defer_fn(function()
            -- go to the end of the terminal buffer
            vim.cmd.startinsert()
          end, 10)
        '';
    };

    home.file.".config/Thunar/uca.xml" = let
      file-roller = "${pkgs.file-roller}/bin/file-roller";
    in
      lib.mkIf (config.mHomeManager.kitty.default) {
        text = ''
          <?xml version="1.0" encoding="UTF-8"?>
          <actions>
          <action>
          <icon>utilities-terminal</icon>
          <name>Open Terminal Here</name>
          <submenu></submenu>
          <unique-id>1704934536132743-1</unique-id>
          <command>${term} --working-directory %f</command>
          <description>Example for a custom action</description>
          <range></range>
          <patterns>*</patterns>
          <startup-notify/>
          <directories/>
          </action>
          <action>
          <icon></icon>
          <name>Compress...</name>
          <submenu></submenu>
          <unique-id>1717492753294488-1</unique-id>
          <command>${file-roller} -d %F</command>
          <description>Compress selected</description>
          <range>*</range>
          <patterns>*</patterns>
          <directories/>
          <audio-files/>
          <image-files/>
          <other-files/>
          <text-files/>
          <video-files/>
          </action>
          <action>
          <icon></icon>
          <name>Extract...</name>
          <submenu></submenu>
          <unique-id>1717493510995203-2</unique-id>
          <command>${file-roller} -f %f</command>
          <description></description>
          <range>*</range>
          <patterns>*.zip;*.7z;*.tar;*.tar.gz;*.rar</patterns>
          <other-files/>
          </action>
          </actions>
        '';
        force = true;
      };
  };
}
