{
  pkgs,
  inputs,
  config,
  lib,
  mLib,
  ...
}: let
  cfg = config.mHomeManager;

  # Taking all modules in ./features and adding enables to them
  features =
    mLib.extendModules
    (name: {
      extraOptions = {
        mHomeManager.${name}.enable = lib.mkEnableOption "enables ${name} configuration";
      };

      configExtension = config: (lib.mkIf cfg.${name}.enable config);
    })
    (mLib.filesIn ./features);

  # Taking all module bundles in ./bundles and adding bundle.enables to them
  bundles =
    mLib.extendModules
    (name: {
      extraOptions = {
        mHomeManager.bundles.${name}.enable = lib.mkEnableOption "enables ${name} module bundle";
      };

      configExtension = config: (lib.mkIf cfg.bundles.${name}.enable config);
    })
    (mLib.filesIn ./bundles);
in {
  imports =
    []
    ++ features
    ++ bundles;

  options = {
    graphical.option = lib.mkOption {
      type = lib.types.bool;
      default = false;
    };
  };

  config = {
    home.stateVersion = "24.05";
  };
}
