{
  pkgs,
  inputs,
  outputs,
  user,
  ...
}: {
  home = {
    username = user;
    homeDirectory = "/home/${user}";
  };

  nix = {
    package = pkgs.nix;
    settings = {
      extra-experimental-features = ["flakes" "nix-command"];
    };
  };

  targets.genericLinux.enable = true;

  mHomeManager = {
    bundles.general.enable = true;
    nvim.enable = true;
  };
}
