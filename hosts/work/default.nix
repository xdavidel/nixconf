# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  pkgs,
  inputs,
  ...
}: let
  hostname = "seddnix";
in {
  imports = [
    ./hardware-configuration.nix
  ];

  mNixOS = {
    bundles.general.enable = true;
    bundles.developer.enable = true;
    bundles.desktop.enable = true;
    bundles.laptop.enable = true;
    bundles.re.enable = true;
    bundles.virt.enable = true;
    bundles.home.enable = true;
    bundles.docker.enable = true;
    bundles.wine.enable = true;
    bundles.nix-ld.enable = true;
    bundles.awesome.enable = true;

    services.bluetooth.enable = true;
    services.ssd.enable = true;

    greetd.enable = true;
    seahorse.enable = true;
    x11.enable = true;
    hyprland.enable = true;
    hamachi.enable = true;
    network.enable = true;
    onlyoffice.enable = true;
    bigram.enable = true;
    zsh.enable = true;

    userName = "User";
    userConfig = ./home.nix;
  };

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.configurationLimit = 5;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.kernelPackages = pkgs.linuxPackages_zen;

  networking.hostName = hostname;

  security.pki.certificateFiles = [
    ./ca-certificates.crt
    ./CatoNetworksTrustedRootCA.pem
  ];

  services.flatpak.enable = true;

  networking.domain = "solaredge.local";

  services.avahi = {
    enable = true;
    publish.enable = true;
    nssmdns4 = true;
  };

  environment.systemPackages = with pkgs; [
    networkmanager-fortisslvpn
    openfortivpn
    obs-studio
    bcompare
    plantuml
    pptp
    remmina
    keepassxc
    teams-for-linux
  ];

  programs.kdeconnect.enable = true;

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  system.stateVersion = "24.05";
}
