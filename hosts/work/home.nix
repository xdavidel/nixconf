{
  inputs,
  outputs,
  pkgs,
  lib,
  ...
}: {
  mHomeManager = {
    bundles.general.enable = true;
    bundles.gtk-qt.enable = true;
    bundles.desktop.enable = true;
    bundles.wayland.enable = true;
    bundles.hyprland.enable = true;
    bundles.music.enable = true;
    bundles.network.enable = true;
    bundles.awesome.enable = true;

    alacritty.enable = true;
    kitty.enable = true;
    kitty.default = true;
    firefox.enable = true;
    dunst.enable = true;
    evince.enable = true;
    brave.enable = true;
    mpv.enable = true;
    nvim.enable = true;
    syncthing.enable = true;
  };
}
