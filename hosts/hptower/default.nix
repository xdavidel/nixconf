{
  pkgs,
  config,
  ...
}: {
  imports = [
    ./hardware-configuration.nix
  ];

  mNixOS = {
    bundles.general.enable = true;
    bundles.developer.enable = true;
    bundles.desktop.enable = true;
    bundles.virt.enable = true;
    bundles.home.enable = true;
    bundles.wine.enable = true;

    x11.enable = true;
    plasma.enable = true;
    network.enable = true;
    zsh.enable = true;
    nvidia.enable = true;

    userName = "User";
    userConfig = ./home.nix;
  };

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.configurationLimit = 5;
  boot.loader.efi.canTouchEfiVariables = true;

  # Optionally, you may need to select the appropriate driver version for your specific GPU.
  hardware.nvidia.package = config.boot.kernelPackages.nvidiaPackages.legacy_470;

  networking.hostName = "homenix";

  services.flatpak.enable = true;

  services.openssh.enable = true;

  # Running sudo requires root password
  security.sudo.extraConfig = "Defaults rootpw";

  environment.systemPackages = with pkgs; [
    ntfs3g
    vlc
  ];

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  system.stateVersion = "24.05";
}
