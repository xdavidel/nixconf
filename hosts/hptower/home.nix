{
  inputs,
  outputs,
  pkgs,
  lib,
  ...
}: {
  mHomeManager = {
    bundles.general.enable = true;
    bundles.desktop.enable = true;
    bundles.music.enable = true;
    bundles.network.enable = true;

    alacritty.enable = true;
    alacritty.default = true;
    firefox.enable = true;
    zathura.enable = true;
    brave.enable = true;
    nvim.enable = true;
    syncthing.enable = true;
  };
}
