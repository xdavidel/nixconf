{
  pkgs,
  inputs,
  ...
}: {
  imports = [
    ./hardware-configuration.nix
  ];

  mNixOS = {
    bundles.general.enable = true;
    bundles.developer.enable = true;
    bundles.desktop.enable = true;
    bundles.cosmic.enable = true;
    bundles.re.enable = true;
    bundles.virt.enable = true;
    bundles.home.enable = true;

    services.bluetooth.enable = true;
    services.ssd.enable = true;

    network.enable = true;
    bigram.enable = true;
    zsh.enable = true;

    userName = "User";
    userConfig = ./home.nix;
  };

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.configurationLimit = 5;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.kernelPackages = pkgs.linuxPackages_zen;

  networking.hostName = "tp-dd";

  system.stateVersion = "24.05";
}
