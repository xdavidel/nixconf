{
  inputs,
  outputs,
  pkgs,
  lib,
  ...
}: {
  mHomeManager = {
    bundles.general.enable = true;
    bundles.gtk-qt.enable = true;
    bundles.desktop.enable = true;
    bundles.wayland.enable = true;
    bundles.cosmic.enable = true;
    bundles.music.enable = true;
    bundles.network.enable = true;

    alacritty.enable = true;
    firefox.enable = true;
    zathura.enable = true;
    brave.enable = true;
    mpv.enable = true;
    nvim.enable = true;
    syncthing.enable = true;
  };
}
