{pkgs}: let
  battery = pkgs.callPackage ./battery {};
  clock = pkgs.callPackage ./clock {};
  cpu = pkgs.callPackage ./cpu {};
  diskspace = pkgs.callPackage ./diskspace {};
  heat = pkgs.callPackage ./heat {};
  memory = pkgs.callPackage ./memory {};
  music = pkgs.callPackage ./music {};
  nettraf = pkgs.callPackage ./nettraf {};
  network = pkgs.callPackage ./network {};
  weather = pkgs.callPackage ./weather {};
in
  pkgs.buildEnv {
    name = "sysstat";
    paths = [
      battery
      clock
      cpu
      diskspace
      heat
      memory
      music
      nettraf
      network
      weather
    ];
  }
