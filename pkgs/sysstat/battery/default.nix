{pkgs}:
pkgs.writeShellApplication {
  name = "battery";
  runtimeInputs = with pkgs; [
    coreutils
    gawk
    gnused
    libnotify
    procps
    util-linux
  ];
  text = builtins.readFile ./battery;
}
