{pkgs}:
pkgs.writeShellApplication {
  name = "diskspace";
  runtimeInputs = with pkgs; [
    coreutils
    curl
    gawk
    libnotify
    procps
    util-linux
  ];
  text = builtins.readFile ./diskspace;
}
