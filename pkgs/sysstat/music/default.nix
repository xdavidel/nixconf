{pkgs}:
pkgs.writeShellApplication {
  name = "music";
  runtimeInputs = with pkgs; [
    coreutils
    gawk
    libnotify
    mpc-cli
    ncmpcpp
    procps
    util-linux
  ];
  text = builtins.readFile ./music;
}
