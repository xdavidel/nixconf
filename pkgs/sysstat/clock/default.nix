{pkgs}:
pkgs.writeShellApplication {
  name = "clock";
  runtimeInputs = with pkgs; [
    coreutils
    gawk
    calcurse
    libnotify
    procps
    util-linux
  ];
  text = builtins.readFile ./clock;
}
