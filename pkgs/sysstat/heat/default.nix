{pkgs}:
pkgs.writeShellApplication {
  name = "heat";
  runtimeInputs = with pkgs; [
    coreutils
    gawk
    gnused
    libnotify
    procps
    util-linux
    lm_sensors
  ];
  text = builtins.readFile ./heat;
}
