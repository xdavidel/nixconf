{pkgs}:
pkgs.writeShellApplication {
  name = "weather";
  runtimeInputs = with pkgs; [
    coreutils
    curl
    gawk
    libnotify
    procps
    util-linux
  ];
  text = builtins.readFile ./weather;
}
