{pkgs}:
pkgs.writeShellApplication {
  name = "mem";
  runtimeInputs = with pkgs; [
    coreutils
    gawk
    libnotify
    procps
    util-linux
  ];
  text = builtins.readFile ./memory;
}
