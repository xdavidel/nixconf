{pkgs}:
pkgs.writeShellApplication {
  name = "network";
  runtimeInputs = with pkgs; [
    coreutils
    curl
    gawk
    libnotify
    procps
    util-linux
  ];
  text = builtins.readFile ./network;
}
