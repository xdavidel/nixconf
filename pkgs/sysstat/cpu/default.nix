{pkgs}:
pkgs.writeShellApplication {
  name = "cpu";
  runtimeInputs = with pkgs; [
    coreutils
    gawk
    libnotify
    procps
    util-linux
  ];
  text = builtins.readFile ./cpu;
}
