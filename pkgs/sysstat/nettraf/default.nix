{pkgs}:
pkgs.writeShellApplication {
  name = "nettraf";
  runtimeInputs = with pkgs; [
    coreutils
    gawk
    iproute2
    gnused
    libnotify
    procps
    util-linux
  ];
  text = builtins.readFile ./nettraf;
}
