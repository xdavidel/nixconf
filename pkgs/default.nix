{pkgs ? import <nixpkgs> {}}: {
  # Personal scripts
  autostart = pkgs.callPackage ./autostart {};
  binrev = pkgs.callPackage ./binrev {};
  bulkmv = pkgs.callPackage ./bulkmv {};
  chs = pkgs.callPackage ./chs {};
  dmc = pkgs.callPackage ./dmc {};
  flatdir = pkgs.callPackage ./flatdir {};
  getwal = pkgs.callPackage ./getwal {};
  hashes = pkgs.callPackage ./hashes {};
  hyprtools = pkgs.callPackage ./hyprtools {};
  iconsel = pkgs.callPackage ./iconsel {};
  likecritty = pkgs.callPackage ./likecritty {};
  listfiles = pkgs.callPackage ./listfiles {};
  nmctl = pkgs.callPackage ./nmctl {};
  pjump = pkgs.callPackage ./pjump {};
  setbg = pkgs.callPackage ./setbg {};
  sysact = pkgs.callPackage ./sysact {};
  sysstat = pkgs.callPackage ./sysstat {};
  vcompress = pkgs.callPackage ./vcompress {};
  x11 = pkgs.callPackage ./x11 {};
}
