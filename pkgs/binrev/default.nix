{pkgs}:
pkgs.writeShellApplication {
  name = "binrev";
  runtimeInputs = with pkgs; [
    coreutils
    gawk
    gnused
    libnotify
    wl-clipboard
    xclip
  ];
  text = builtins.readFile ./binrev;
}
