{pkgs}:
pkgs.writeShellApplication {
  name = "autostart";
  runtimeInputs = with pkgs; [
    procps
    swww
    xdg-user-dirs
  ];
  text = builtins.readFile ./autostart;
}
