{pkgs}: let
  maimpick = pkgs.callPackage ./maimpick {};
  xsetbg = pkgs.callPackage ./xsetbg {};
in
  pkgs.buildEnv {
    name = "x11";
    paths = [
      maimpick
      xsetbg
    ];
  }
