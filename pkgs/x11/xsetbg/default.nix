{pkgs}:
pkgs.writeShellApplication {
  name = "xsetbg";
  runtimeInputs = with pkgs; [
    coreutils
    feh
    findutils
    libnotify
    procps
    util-linux
  ];
  text = builtins.readFile ./xsetbg;
}
