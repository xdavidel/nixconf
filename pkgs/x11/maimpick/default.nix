{pkgs}:
pkgs.writeShellApplication {
  name = "maimpick";
  runtimeInputs = with pkgs; [
    bemenu
    coreutils
    maim
    xclip
    xdotool
  ];
  text = builtins.readFile ./maimpick;
}
