{pkgs}: let
  git = "${pkgs.git}/bin/git";
  find = "${pkgs.findutils}/bin/find";
  sed = "${pkgs.gnused}/bin/sed";
  fzf = "${pkgs.fzf}/bin/fzf";
in
  pkgs.writeScriptBin "listfiles"
  /*
  sh
  */
  ''
    #!/bin/sh

    viewer() {
        ${fzf} --preview='${pkgs.ctpv}/bin/ctpv {}' \
            --preview-window='up:70%:wrap' \
            --bind "enter:become($EDITOR {})"
    }

    root_path="$(${git} rev-parse --show-toplevel 2>/dev/null)"

    if [ -n "$root_path" ]; then
        cd "$root_path"
        ${git} ls-files --full-name | viewer
    else
        ${find} -L . -maxdepth 5 -type f 2>/dev/null | ${sed} 's|^./||g' | viewer
    fi
  ''
