{pkgs}:
pkgs.writeShellApplication {
  name = "nmctl";
  runtimeInputs = with pkgs; [
    bemenu
    coreutils
    getopt
    gnugrep
    util-linux
  ];
  text = builtins.readFile ./nmctl;
}
