{pkgs}:
pkgs.writeShellApplication {
  name = "pjump";
  runtimeInputs = with pkgs; [
    coreutils
    jq
  ];
  text = builtins.readFile ./pjump;
}
