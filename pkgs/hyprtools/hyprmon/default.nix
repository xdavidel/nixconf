{pkgs}:
pkgs.writeShellApplication {
  name = "hyprmon";
  runtimeInputs = with pkgs; [
    coreutils
    xdg-utils
    gnused
    jq
    socat
    wdisplays
  ];
  text = builtins.readFile ./hyprmon;
}
