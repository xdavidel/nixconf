{pkgs}: let
  hyprmon = pkgs.callPackage ./hyprmon {};
  screenshot = pkgs.callPackage ./screenshot {};
  togglefloatfocus = pkgs.callPackage ./togglefloatfocus {};
in
  pkgs.buildEnv {
    name = "hyprtools";
    paths = [
      hyprmon
      screenshot
      togglefloatfocus
    ];
  }
