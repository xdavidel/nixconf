# Nix Configuration

This is a renewed modular configuration for Nix.

## NixOS

### Partitioning

First we need to create the partitions for
our system.

This include at least a `boot` partition and
a `root` partition.

After that we need to create a file system
for each partition:

```sh
mkfs.ext4 -L nixos /dev/sda2
mkfs.fat -F32 -n boot /dev/sda1
```

Now we mount the partitions:

```sh
mount /dev/sda2 /mnt
mkdir -p /mnt/boot/efi
mount /dev/sda1 /mnt/boot/efi
```

Finally we create the needed `hardware-configuration.nix` file:

```sh
nixos-generate-config
```

And we copy this file here.

### Setup Host

We use this command to apply host from the flake:

```sh
nixos-install --flake .#<host>
```

### Rebuilding

```sh
nixos-rebuild --flake .#<host> switch
```

## Shell

To use a shell with all custom packages in root directory of project:

1. Using nix-channels:

```sh
nix-shell
```


1. Using flakes:

```sh
nix develop
```
